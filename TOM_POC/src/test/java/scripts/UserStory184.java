package scripts;

import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class UserStory184 
{
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;
	@Test
	public void call() throws MalformedURLException, InterruptedException 
	{
	DesiredCapabilities cap=new DesiredCapabilities();
	System.setProperty("webdriver.http.factory", "apache");
	cap=new DesiredCapabilities();
	cap.setCapability("platformName","Android");
	cap.setCapability("platformVersion","7");
	cap.setCapability("appPackage","com.paycraft.tom");
	cap.setCapability("appActivity","com.paycraft.tom.splash.SplashActivity");
	cap.setCapability("deviceName","T2mini");
	cap.setCapability("automationName","Appium");
	cap.setCapability("UDID","TN04197E40003");
	//cap.setCapability("autoGrantPermissions",true);
	dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
	dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	LoginPage loginPage = new LoginPage(dr);
	rm=new ReusableMethods();
	loginPage.getMockoRadioBtn().click();
	loginPage.getProceedBtn().click();
	Thread.sleep(1000);
	hp=new HomePage(dr);
	rm=new ReusableMethods();
	Reporter.log("Login successful!",true);
	try 
	{
		hp.getBluetoothIcon().click();
		hp.clickingEDC();
		try
		{
		hp.getCancelBTN().click();
		Reporter.log("Connected device name : "+hp.getBluetoothConnectedDeviceName().getText(), true);
		
		}
		catch (NoSuchElementException e) {
			Reporter.log("Exception occure while clicking on cancel button of bluetooth ",true);
		}

	}
	catch (NoAlertPresentException e) 
	{
		Reporter.log("Bluetooth connection alert not displayed",true);
	}
		try 
		{
			Thread.sleep(2000);
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		

		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Enter manually customer detail option displayed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("enter customer detail manualy page displayed",true);
					cdm.getDropDownArrow().click();
					cdm.getFirstNameDropDownOption().click();
					cdm.setFirstNameTB("dhanashree");
					dr.hideKeyboard();
					cdm.setLastNameTB("hegde");
					dr.hideKeyboard();
					cdm.getRadioBtn().click();
					cdm.getRadioBtn().click();
					cdm.setDobTB("12/02/1993");
					dr.hideKeyboard();
					cdm.clickingselectGender();
					cdm.selectFema();
					cdm.setIdNumTB("452131595950");
					
					dr.hideKeyboard();
					cdm.setOTPTB("1234");
					dr.hideKeyboard();
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							cdm.getConfirmOTPBTN().click();
							Thread.sleep(2000);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on confirm otp button",true);
					}
				}

				catch (Exception e) 
				{
					Reporter.log("Exception occured while entering customer detail manualy ",true);
				}
					//cdm.clickingVerifyBTN();
					stp=new StoreValuePage(dr);
					Thread.sleep(2000);
					try 
					{
						
						Reporter.log("store value page displayed",true);
					
					stp.setStoreValueField("250");
					try 
					{
						if(stp.getPayBTN().isEnabled())
						{	
							stp.getPayBTN().click();
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("set amount first",true);
						try 
						{
							stp.getPayBTN().click();
						}
						catch (Exception ex) 
						{
							Reporter.log("pay button not displayed",true);
						}
					}
					
					}
					catch(Exception e) 
					{
						Reporter.log("Exception in store value page ",true);
					}
					pp=new PaymentPage(dr);
					try {
						if(pp.getConfirmBTN().isDisplayed())
						{
							Reporter.log("Payment page displayed",true);
							Reporter.log("Acceptance criteria "+rm.readExcel("AM-184",1,2 )+"Passed ------>",true);
							String storevalue=pp.getStoreValueSet().getText();
							String totalamount=pp.getTotalAmountSet().getText();
							String cardfee=pp.getCardFeeSet().getText();
							Reporter.log("Card Fee displayed as: "+cardfee,true);
							Reporter.log("Store Value displayed as: "+storevalue,true);
							Reporter.log("Total amount displayed as: "+totalamount,true);
							Thread.sleep(3000);
						}
					} 
					catch (Exception e) {
						Reporter.log("Acceptance criteria "+rm.readExcel("AM-184",1,2 )+"Failed ------>",true);
						Reporter.log("Exception occured in payment page ",true);
					}
			}
		}
		catch (Exception e) {
			Reporter.log("Enter manually customer detail option not displayed",true);
		}
	}
}
