package scripts;

import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class UserStoryAM96 {
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;

	@Test
	public void call() throws MalformedURLException, InterruptedException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.splash.SplashActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		loginPage.getMockoRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		Reporter.log("Login successful!",true);
		Reporter.log("----------User Story AM-96----------", true);
		try 
		{
			hp.getBluetoothIcon().click();
			//hp.clickingEDC();
			dr.findElementByXPath("//android.widget.TextView[contains(@text, 'Redmi')]").click();
			try
			{
			hp.getCancelBTN().click();
			Reporter.log("Connected device name : "+hp.getBluetoothConnectedDeviceName().getText(), true);
			
			}
			catch (NoSuchElementException e) {
				Reporter.log("Exception occure while clicking on cancel button of bluetooth ",true);
			}

		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		try 
		{
			Thread.sleep(2000);
			//hp.typeCRN(rm.getPropertyData("CRN"));
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		
		

		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Enter manually customer detail option displayed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("enter customer detail manualy page displayed",true);
					cdm.getDropDownArrow().click();
					cdm.getFirstNameDropDownOption().click();
					cdm.setFirstNameTB("dhanashree");
					dr.hideKeyboard();
					cdm.setLastNameTB("hegde");
					dr.hideKeyboard();
					cdm.getRadioBtn().click();
					cdm.getRadioBtn().click();
					cdm.setDobTB("12/02/1993");
					dr.hideKeyboard();
					cdm.clickingselectGender();
					cdm.selectFema();
					cdm.setIdNumTB("452131595950");
					Thread.sleep(1000);

//					dr.hideKeyboard();
					cdm.setOTPTB("1234");
					dr.hideKeyboard();
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							cdm.getConfirmOTPBTN().click();
							try 
							{
								
								MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
								Reporter.log("Server response/Toast message is: "+toast.getText(),true);
		
								rm.takeScreenShot(dr);
							}
							catch (NoSuchElementException ex) 
							{
								Reporter.log("toast not displayed",true);
							}
							Thread.sleep(2000);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on confirm otp button",true);
					}
				}

				catch (Exception e) 
				{
					Reporter.log("Exception occured while entering customer detail manualy ",true);
				}
					//cdm.clickingVerifyBTN();
					stp=new StoreValuePage(dr);
					Thread.sleep(2000);
					try 
					{
						
						Reporter.log("store value page displayed",true);		
					//Reporter.log("Preconfigured top up amounts displayed as "+stp.getBTN100().getText()+" "+stp.getBTN200().getText()+" "+stp.getBTN300().getText()+" "+stp.getBTN400().getText()+" "+stp.getBTN500().getText(),true);
					stp.clickingBTN100();
					//Reporter.log("Amount set by selecting on Preconfigured topup amount is "+stp.getStoreValueField().getText(),true);
					stp.clickingClearBTN();
					stp.setStoreValueField("300");
					dr.hideKeyboard();
				//	Reporter.log("Amount set by entering topup amount is "+stp.getStoreValueField().getText(),true);
					
					try 
					{
						if(stp.getPayBTN().isDisplayed())
						{	
							Reporter.log("pay button displayed",true);
						stp.getPayBTN().click();
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("set amount first",true);
						try 
						{
							stp.getPayBTN().click();
						}
						catch (Exception ex) 
						{
							Reporter.log("pay button not displayed",true);
						}
					}
					
					}
					catch(Exception e) 
					{
						Reporter.log("Exception in store value page ",true);
					}
					pp=new PaymentPage(dr);
					try {
						if(pp.getConfirmBTN().isDisplayed())
						{
							Reporter.log("payment page displayed",true);
							pp.clickingCashBTN();
							String totalamount=pp.getTotalAmountSet().getText();
							Reporter.log("Total amount displayed is "+totalamount);
							Thread.sleep(1000);
							pp.setCashReceived("1000");
							Reporter.log("change to be return "+ pp.getChangeField().getText(),true);
							pp.clickingConfirmBTN();
					
							
							psp=new PaymentSuccessfulPage(dr);
							Thread.sleep(500);
							try
							{
								if(psp.getPaymentReceivedSuccessfullyMsg().isDisplayed()&&psp.getCardIssuedAndLinkedSuccessfullyMsg().isDisplayed()&&psp.getHostTopUpDoneSuccessfullyMsg().isDisplayed())
								{
									//System.out.println(psp.getPaymentReceivedSuccessfullyMsg().getText());
									if(psp.getPaymentReceivedSuccessfullyMsg().getText().equals("Payment received successfully!")&&psp.getCardIssuedAndLinkedSuccessfullyMsg().getText().equals("Store Value Card issued and linked successfully!")&&psp.getHostTopUpDoneSuccessfullyMsg().getText().equals("Host top up done successfully!"))
									{
										Reporter.log("Message displayed as: "+psp.getPaymentReceivedSuccessfullyMsg().getText()+" "+psp.getCardIssuedAndLinkedSuccessfullyMsg().getText()+" "+psp.getHostTopUpDoneSuccessfullyMsg().getText(),true);
									try
									{
										Thread.sleep(3000);
										if(dr.findElement(By.xpath("//android.widget.TextView[@index='0']")).isDisplayed())
										{
											
											String cardlinkmsg=dr.findElement(By.xpath("//android.widget.TextView[@index='0']")).getText();
											
											if(cardlinkmsg.equalsIgnoreCase("Card linked successfully. Please wait while PIN is being set"))
											{
												Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-96",2 ,2 )+" Passed------>",true);
											Reporter.log("Message displayed as: "+cardlinkmsg,true);
											}
											else
											{
												Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-96",2 ,2 )+" Failed------>",true);
												Reporter.log("Card linked successfully. Please wait while PIN is being set Message NOT dispalyed",true);
											}
											Thread.sleep(12000);
											try
											{
												
												if(psp.getconfirmBTN().isDisplayed())
												{
													
													String cardissuedmsg=dr.findElement(By.xpath("//android.widget.TextView[@index='1']")).getText();
													if(cardissuedmsg.equalsIgnoreCase("Card issued and PIN SET done successfully"))
													{
														Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-96",3 ,2 )+" Passed------>",true);
														Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-96",4 ,2 )+" Passed------>",true);
														String cardbalancetext=psp.getCardBalance().getText();
														String cardbalance=cardbalancetext.replace("Card balance is ₹", "");
													Reporter.log("Message displayed as: "+cardissuedmsg,true);
													Reporter.log("Card Balance is : "+cardbalance,true);
													
													psp.getconfirmBTN().click();
													}
													else if(cardissuedmsg.equalsIgnoreCase("PIN Set and Chip Balance update failed"))
													{
														Reporter.log("Exception Criteria---------- "+rm.readExcel("AM-96",3 ,4 )+" Passed------>",true);
													Reporter.log("Message displayed as: "+cardissuedmsg,true);
													
													psp.getconfirmBTN().click();
													}
													else
													{
														
														Reporter.log("Message displayed as: "+cardissuedmsg,true);
													}
												
												}
												
											}
											catch (NoSuchElementException e) {
												Reporter.log("Card issued and PIN SET not done successfully",true);
											}
										}
										
									}
									catch (NoSuchElementException e) 
									{
										Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-96",2 ,2 )+" Failed------>",true);
									}	
								}
							else
									{
										
										Reporter.log("Payment successful message not displayed",true);
										try 
										{
											pp.clickingConfirmBTN();
											MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
											String toastmsg=dr.findElementByXPath("//android.widget.Toast[1]").getText();
											if(toastmsg.equalsIgnoreCase("EDC not connected"))
											{
											Reporter.log("Server response/Toast message is: "+toastmsg,true);
											}
											else if((toastmsg.equalsIgnoreCase("Card linking failed")))
											{
												Reporter.log("Exception criteria---------- "+rm.readExcel("AM-96",1 ,4 )+" Passed------>",true);
												Reporter.log("Server response/Toast message is: "+toastmsg,true);
											}
											else
											{
												Reporter.log("Server response/Toast message is: "+toastmsg,true);
											}
											rm.takeScreenShot(dr);
										}
										catch (NoSuchElementException ex) 
										{
											Reporter.log("Payment successful message not displayed but tost message not displayed",true);
										}
										
									}
								}
								
							
							}
							catch (NoSuchElementException e)
							{
								Reporter.log("Payment successful message not displayed",true);
								try 
								{
									pp.clickingConfirmBTN();
									MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
									String toastmsg=dr.findElementByXPath("//android.widget.Toast[1]").getText();
									if(toastmsg.equalsIgnoreCase("EDC not connected"))
									{
										Reporter.log("Server response/Toast message is: "+toastmsg,true);
									}
									else if((toastmsg.equalsIgnoreCase("Card linking failed")))
									{
										Reporter.log("Exception criteria 1 Passed------>",true);
										Reporter.log("Server response/Toast message is: "+toastmsg,true);
									}
									else
									{
										Reporter.log("Server response/Toast message is: "+toastmsg,true);
									}
									rm.takeScreenShot(dr);
								}
								catch (NoSuchElementException ex) 
								{
									Reporter.log("edc toast msg not displayed",true);
								}

						   }
					}
					}
					catch (Exception e) {
						Reporter.log("Exception occured in payment page ",true);
					}
			}
		}
		catch (Exception e) {
			Reporter.log("Exception in enter customer detail manually",true);
		}
		
		

	}

}
