package scripts;

import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class UserStory185 
{
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;

	@Test
	public void call() throws MalformedURLException, InterruptedException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.splash.SplashActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		loginPage.getMockoRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		Reporter.log("Login successful!",true);
		Reporter.log("User Story AM-185", true);
		try 
		{
			hp.getBluetoothIcon().click();
			hp.clickingEDC();
			try
			{
			hp.getCancelBTN().click();
			Reporter.log("Connected device name : "+hp.getBluetoothConnectedDeviceName().getText(), true);
			
			}
			catch (NoSuchElementException e) {
				Reporter.log("Exception occure while clicking on cancel button of bluetooth ",true);
			}

		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		try 
		{
			Thread.sleep(2000);
			//hp.typeCRN(rm.getPropertyData("CRN"));
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		
		

		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Enter manually customer detail option displayed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("enter customer detail manualy page displayed",true);
					cdm.getDropDownArrow().click();
					cdm.getFirstNameDropDownOption().click();
					cdm.setFirstNameTB("dhanashree");
					dr.hideKeyboard();
					cdm.setLastNameTB("hegde");
					dr.hideKeyboard();
					cdm.getRadioBtn().click();
					cdm.getRadioBtn().click();
					cdm.setDobTB("12/02/1993");
					dr.hideKeyboard();
					cdm.clickingselectGender();
					cdm.selectFema();
					cdm.setIdNumTB("452131595950");
					Thread.sleep(1000);

//					dr.hideKeyboard();
					cdm.setOTPTB("1234");
					dr.hideKeyboard();
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							cdm.getConfirmOTPBTN().click();
							try 
							{
								
								MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
								Reporter.log("Server response/Toast message is: "+toast.getText(),true);
		
								rm.takeScreenShot(dr);
							}
							catch (NoSuchElementException ex) 
							{
								Reporter.log("toast not displayed",true);
							}
							Thread.sleep(2000);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on Verify button",true);
					}
				}

				catch (Exception e) 
				{
					Reporter.log("Exception occured while entering customer detail manualy ",true);
				}
					//cdm.clickingVerifyBTN();
					stp=new StoreValuePage(dr);
					Thread.sleep(2000);
					try 
					{
						
						Reporter.log("store value page displayed",true);		
					//Reporter.log("Preconfigured top up amounts displayed as "+stp.getBTN100().getText()+" "+stp.getBTN200().getText()+" "+stp.getBTN300().getText()+" "+stp.getBTN400().getText()+" "+stp.getBTN500().getText(),true);
					stp.clickingBTN100();
					//Reporter.log("Amount set by selecting on Preconfigured topup amount is "+stp.getStoreValueField().getText(),true);
					stp.clickingClearBTN();
					stp.setStoreValueField("300");
					dr.hideKeyboard();
				//	Reporter.log("Amount set by entering topup amount is "+stp.getStoreValueField().getText(),true);
					
					try 
					{
						if(stp.getPayBTN().isDisplayed())
						{	
							Reporter.log("pay button displayed",true);
						stp.getPayBTN().click();
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("set amount first",true);
						try 
						{
							stp.getPayBTN().click();
						}
						catch (Exception ex) 
						{
							Reporter.log("pay button not displayed",true);
						}
					}
					
					}
					catch(Exception e) 
					{
						Reporter.log("Exception in store value page ",true);
					}
					pp=new PaymentPage(dr);
					try {
						if(pp.getConfirmBTN().isDisplayed())
						{
							Reporter.log("payment page displayed",true);
							pp.clickingCashBTN();
							String totalamount=pp.getTotalAmountSet().getText();
							String total=totalamount.replace("₹", "");
							int totalInt=Integer.parseInt(total);
							Reporter.log("Total amount displayed as "+totalInt,true);
							String cashreceived=pp.getCashReceivedTB().getText();
									int cash=Integer.parseInt(cashreceived);
							Reporter.log("Cash received displayed as "+cash,true);
							Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",1 ,2 )+" Passed------>",true);
							Thread.sleep(1000);
							if(totalInt==cash)
							{
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",2 ,2 )+" Passed------>",true);
							}
							else
							{
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",2 ,2 )+" Failed------>",true);
							}
							Reporter.log("Cash Received before edit: "+cashreceived,true);
							pp.setCashReceived("1000");
							
							String cashreceived1=pp.getCashReceivedTB().getText();
									Reporter.log("Cash Received after edit: "+cashreceived1,true);	
							
							if(cashreceived.equals(cashreceived1))
							{
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",3 ,2 )+" Failed------>",true);
							}
							else
							{

								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",3 ,2 )+" Passed------>",true);
							}	
							pp.setCashReceived("1000");
							String cashreceived2=pp.getCashReceivedTB().getText();
									int cash1=Integer.parseInt(cashreceived2);
							String changeoriginal=pp.getChangeField().getText();
							String change=changeoriginal.replace("₹", "");
							int changeamount=Integer.parseInt(change);
							if(cash1-totalInt==changeamount)
							{
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",4 ,2 )+" Passed------>",true);
							}
									
									else
									{
										Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",4 ,2 )+" Failed------>",true);
									}	
							Reporter.log("change to be return "+changeamount ,true);
							pp.clickingConfirmBTN();
							Thread.sleep(1000);
							
							psp=new PaymentSuccessfulPage(dr);
							
							try
							{
								if(psp.getPaymentReceivedSuccessfullyMsg().isDisplayed()&&psp.getCardIssuedAndLinkedSuccessfullyMsg().isDisplayed()&&psp.getHostTopUpDoneSuccessfullyMsg().isDisplayed())
								{
									if(psp.getPaymentReceivedSuccessfullyMsg().getText().equals("Payment received successfully!")&&psp.getCardIssuedAndLinkedSuccessfullyMsg().getText().equals("Store Value Card issued and linked successfully!")&&psp.getHostTopUpDoneSuccessfullyMsg().getText().equals("Host top up done successfully!"))
									{
										Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",5 ,2 )+" Passed------>",true);
										Reporter.log("Message displayed as: "+psp.getPaymentReceivedSuccessfullyMsg().getText()+" "+psp.getCardIssuedAndLinkedSuccessfullyMsg().getText()+" "+psp.getHostTopUpDoneSuccessfullyMsg().getText(),true);
									
									}
									
									
									else
									{
										Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",5 ,2 )+" Failed------>",true);
										Reporter.log("Payment successful message not displayed",true);
										
									}
								}
								
							
							}
							catch (NoSuchElementException e)
							{
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-185",5 ,2 )+" Failed------>",true);
								Reporter.log("Payment successful message not displayed",true);
								

						   }
					}
					}
					catch (Exception e) {
						Reporter.log("Exception occured in payment page ",true);
					}
			}
		}
		catch (Exception e) {
			Reporter.log("Enter manually customer detail option not displayed",true);
		}
		
	}
}
