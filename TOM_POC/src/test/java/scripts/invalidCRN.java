package scripts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import baseLibrary.VerhoeffAlgorithm;
import io.appium.java_client.android.AndroidDriver;
import pom.HomePage;
import pom.LoginPage;

public class invalidCRN {
	
		AndroidDriver dr;
		ReusableMethods rm;
		LoginPage loginPage;
		HomePage hp ;
		VerhoeffAlgorithm va;
		
		DesiredCapabilities cap=new DesiredCapabilities();
		@BeforeTest
		public void call() throws MalformedURLException 
		{
			
			DesiredCapabilities cap=new DesiredCapabilities();
			System.setProperty("webdriver.http.factory", "apache");
			cap=new DesiredCapabilities();
			cap.setCapability("platformName","Android");
			cap.setCapability("platformVersion","7");
			cap.setCapability("appPackage","com.paycraft.tom");
			cap.setCapability("appActivity","com.paycraft.tom.login.LoginActivity");
			cap.setCapability("deviceName","T2mini");
			cap.setCapability("automationName","Appium");
			cap.setCapability("UDID","TN04197E40003");
			//cap.setCapability("autoGrantPermissions",true);
			dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
			dr.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		}
		
		@Test(priority = 1)
		public void login() throws InterruptedException, IOException 
		{

			LoginPage loginPage = new LoginPage(dr);
			hp=new HomePage(dr);
			rm=new ReusableMethods();
			dr.hideKeyboard();
			loginPage.getTom_v1BTN().click();
			loginPage.getMockRadioBtn().click();
			loginPage.getProceedBtn().click();
			Thread.sleep(1000);
			loginPage.typeUsername(rm.getPropertyData("username"));
			dr.hideKeyboard();


			loginPage.getPasswordTB().click();
			dr.hideKeyboard();
			loginPage.typePassword(rm.getPropertyData("password"));
			loginPage.clickingSubmit();
			Thread.sleep(3000);
		}
		@Test(priority = 2,dependsOnMethods ="login")
		public void createPass() throws InterruptedException, IOException 
		{
			Thread.sleep(2000);
			hp=new HomePage(dr);
			rm=new ReusableMethods();
			va=new VerhoeffAlgorithm();
			Reporter.log("Login successful!",true);
			try 
			{
				Alert alert = dr.switchTo().alert();
				alert.dismiss();



			}
			catch (NoAlertPresentException e) 
			{
				Reporter.log("Bluetooth connection alert not displayed",true);
			}
		}
		@Test(priority = 3)
		public void emptyCRNAndMob() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("");
			dr.hideKeyboard();
			hp.typeMobileNumber("");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:-------CRN and Mobile Num fields are empty-------",true);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:------CRN and Mobile Num fields are empty-------",true);
				}
			
		}
		@Test(priority = 4)
		public void emptyMob() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("ASDFGHSF1234");
			dr.hideKeyboard();
			hp.typeMobileNumber("");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:--------Mobile Num field is empty------------",true);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:---------Mobile Num field is empty--------",true);
				}
			
		}
		
		@Test(priority = 5)
		public void alphaCRN() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("ASDFGHSFMJNJ");
			dr.hideKeyboard();
			hp.typeMobileNumber("9900000000");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:----Entered CRN is in only Alphabets-------",true);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:----Entered CRN is Alphanumeric-------",true);
				}
			
		}
		@Test(priority = 6)
		public void LengthCRN() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("ASDFGHSF12");
			dr.hideKeyboard();
			hp.typeMobileNumber("9800000000");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:----Entered CRN is in 12 digit",true);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:----Entered CRN is length is less than 12 digits",true);
				}
			
		}	
		@Test(priority = 7)
		public void numberCRN() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("123456789101");
			dr.hideKeyboard();
			hp.typeMobileNumber("9900000000");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:----Entered CRN is in only digits-------",true);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:----Entered CRN is Alphanumeric-------",true);
				}
			
		}
		@Test(priority = 8)
		public void aplhaMobileNum() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("ASDF1234ASDF");
			dr.hideKeyboard();
			hp.typeMobileNumber("hjdhfjsjdj");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:----Entered Mobile num is in alphabets-------",true);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:----Entered Mobile Num is in digits-------",true);
				}
			
		}
		@Test(priority = 9)
		public void lengthMobileNum() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("ASDF1234ASDF");
			dr.hideKeyboard();
			hp.typeMobileNumber("98000000");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:----Entered Mobile num is 10 digit-------",true);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:----Entered Mobile Num is in less than 10 digit------",true);
				}
			
		}
		@Test(priority = 10)
		public void specialCharCRN() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("ASDF1234@#$%");
			dr.hideKeyboard();
			hp.typeMobileNumber("9800000000");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:----Entered CRN has the specialchars-------",true);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:----Entered CRN has the specialchars------",true);
				}
			
		}
		@Test(priority = 11)
		public void specialCharMobileNum() throws InterruptedException, IOException 
		{
			Thread.sleep(1000);
			hp=new HomePage(dr);
			hp.typeCRN("ASDF1234ASDF");
			dr.hideKeyboard();
			hp.typeMobileNumber("98000000#@%");
			try
			{
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Test Fail:----Entered mobile num has specialchars-------",true);
					rm.takeScreenShot(dr);
				}
			}
				catch (Exception e) {
					Reporter.log("Test Pass:----Entered mobile num has specialchars------",true);
				}
			
		}
}
