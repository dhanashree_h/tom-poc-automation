package scripts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import baseLibrary.VerhoeffAlgorithm;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class InvalidAdhar {
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;
	VerhoeffAlgorithm va;

	@BeforeTest
	public void call() throws MalformedURLException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.login.LoginActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

	}

	@Test(priority = 1)
	public void login() throws InterruptedException, IOException 
	{

		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		dr.hideKeyboard();
		loginPage.getTom_v1BTN().click();
		loginPage.getMockRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		loginPage.typeUsername(rm.getPropertyData("username"));
		dr.hideKeyboard();


		loginPage.getPasswordTB().click();
		dr.hideKeyboard();
		loginPage.typePassword(rm.getPropertyData("password"));
		loginPage.clickingSubmit();
		Thread.sleep(3000);
	}

	@Test(priority = 2,dependsOnMethods ="login")
	public void createPass() throws InterruptedException, IOException 
	{

		Thread.sleep(2000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		va=new VerhoeffAlgorithm();
		Reporter.log("Login successful!",true);
		try 
		{
			Alert alert = dr.switchTo().alert();
			alert.dismiss();



		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		try 
		{
			Thread.sleep(2000);
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		

		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Enter manually customer detail option displayed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("enter customer detail manualy page displayed",true);
					try 
					{
						
						cdm.getDropDownArrow().click();
						cdm.getFirstNameDropDownOption().click();
						cdm.setFirstNameTB("dhanashree");
						dr.hideKeyboard();
						cdm.setLastNameTB("hegde");
						dr.hideKeyboard();
						cdm.getRadioBtn().click();
						cdm.getRadioBtn().click();
						cdm.setIdNumTB("");
						
						dr.hideKeyboard();
						cdm.setDobTB("12/02/1993");
						dr.hideKeyboard();
                        cdm.setOTPTB("1234");
						dr.hideKeyboard();
						Thread.sleep(1000);
						try {
							if(cdm.getConfirmOTPBTN().isEnabled())
							{
								Reporter.log("Confirm OTP button got enabled :: Test case for Blank Adhar Id TextField is failed ",true);
							}
							else
							{
								Reporter.log("Confirm OTP button not got enabled :: Test case for Blank Adhar Id TextField is passed",true);
							}
						 } 
						catch (Exception e) 
						{
							Reporter.log("something went wrong in manual entry page",true);
						}
						
						cdm.getFirstNameTB().clear();
						cdm.getLastNameTB().clear();
						cdm.getIdNumTB().clear();
					
						cdm.getDobTB().clear();
						cdm.getOTPTB().clear();
						
						
					} 
					catch (Exception e) 
					{
						Reporter.log("something went wrong while entering blank in Adhar Id textfield",true);
					}
					try 
					{
						String data="12345678901";
						cdm.getDropDownArrow().click();
						cdm.getFirstNameDropDownOption().click();
						cdm.setFirstNameTB("dhanashree");
						dr.hideKeyboard();
						cdm.setLastNameTB("hegde");
						dr.hideKeyboard();
						cdm.getRadioBtn().click();
						cdm.getRadioBtn().click();
						cdm.setIdNumTB(data);
						
						dr.hideKeyboard();
						cdm.setDobTB("12/02/1993");
						dr.hideKeyboard();
                        cdm.setOTPTB("1234");
						dr.hideKeyboard();
						Thread.sleep(1000);
						try {
							if(cdm.getConfirmOTPBTN().isEnabled())
							{
								Reporter.log("Confirm OTP button got enabled :: Test case for Adhar Id number count less than Required Adhar id number count is failed ",true);
							}
							else
							{
								Reporter.log("Confirm OTP button not got enabled :: Test case for Adhar Id number count less than Required Adhar id number count is passed",true);
							}
						 } 
						catch (Exception e) 
						{
							Reporter.log("something went wrong in manual entry page",true);
						}
						
						cdm.getFirstNameTB().clear();
						cdm.getLastNameTB().clear();
						cdm.getIdNumTB().clear();
					
						cdm.getDobTB().clear();
						cdm.getOTPTB().clear();
						if(va.validateVerhoeff(data))
						{
							Reporter.log("Adhar Id is passed for Verhoeff Algorithm Standard");
						}
						else
						{
							Reporter.log("Adhar Id is failed for Verhoeff Algorithm Standard");
						}
						
					} 
					catch (Exception e) 
					{
						Reporter.log("something went wrong while entering Adhar Id number count less than Required Adhar id number count in Adhar Id textfield",true);
					}
					try 
					{
						String data="123456789012";
						cdm.getDropDownArrow().click();
						cdm.getFirstNameDropDownOption().click();
						cdm.setFirstNameTB("dhanashree");
						dr.hideKeyboard();
						cdm.setLastNameTB("hegde");
						dr.hideKeyboard();
						cdm.getRadioBtn().click();
						cdm.getRadioBtn().click();
						cdm.setIdNumTB(data);
						
						dr.hideKeyboard();
						cdm.setDobTB("12/02/1993");
						dr.hideKeyboard();
                        cdm.setOTPTB("1234");
						dr.hideKeyboard();
						Thread.sleep(1000);
						try {
							if(cdm.getConfirmOTPBTN().isEnabled())
							{
								Reporter.log("Confirm OTP button got enabled :: Test case for Adhar Id number count exact Required Adhar id number count is  passed",true);
							}
							else
							{
								Reporter.log("Confirm OTP button not got enabled :: Test case for Adhar Id number count exact Required Adhar id number count is failed",true);
							}
						 } 
						catch (Exception e) 
						{
							Reporter.log("something went wrong in manual entry page",true);
						}
						
						cdm.getFirstNameTB().clear();
						cdm.getLastNameTB().clear();
						cdm.getIdNumTB().clear();
					
						cdm.getDobTB().clear();
						cdm.getOTPTB().clear();
						if(va.validateVerhoeff(data))
						{
							Reporter.log("Adhar Id is passed for Verhoeff Algorithm Standard",true);
						}
						else
						{
							Reporter.log("Adhar Id is failed for Verhoeff Algorithm Standard",true);
						}
						
					} 
					catch (Exception e) 
					{
						Reporter.log("something went wrong while entering Adhar Id number count exact Required Adhar id number count in Adhar Id textfield",true);
					}
				for (int i = 0; i<2; i++) 
				{
					
						cdm.getDropDownArrow().click();
						cdm.getFirstNameDropDownOption().click();
						cdm.setFirstNameTB("dhanashree");
						dr.hideKeyboard();
						cdm.setLastNameTB("hegde");
						dr.hideKeyboard();
//						cdm.getRadioBtn().click();
						cdm.getRadioBtn().click();
						cdm.setIdNumTB(rm.readExcel("InvalidAdhar", i, 0));
						
						dr.hideKeyboard();
						cdm.setDobTB("12/02/1993");
						dr.hideKeyboard();
                        cdm.setOTPTB("1234");
						dr.hideKeyboard();
						Thread.sleep(1000);
						try {
							if(cdm.getConfirmOTPBTN().isEnabled())
							{
								Reporter.log("Confirm OTP button got enabled :: Test case for "+rm.readExcel("InvalidAdhar", i, 0)+" is failed ",true);
							}
							else
							{
								Reporter.log("Confirm OTP button not got enabled :: Test case for "+rm.readExcel("InvalidAdhar", i, 0)+" is passed ",true);
							}
						 } 
						catch (Exception e) 
						{
							System.out.println("something went wrong in manual entry page");
						}
						
						cdm.getFirstNameTB().clear();
						cdm.getLastNameTB().clear();
						cdm.getIdNumTB().clear();
					
						cdm.getDobTB().clear();
						cdm.getOTPTB().clear();
						try {
						if(va.validateVerhoeff(rm.readExcel("InvalidAdhar", i, 0)))
						{
							Reporter.log("Adhar Id is passed for Verhoeff Algorithm Standard",true);
						}
						else
						{
							Reporter.log("Adhar Id is failed for Verhoeff Algorithm Standard",true);
						}
						}
						catch (Exception e) {
							Reporter.log("Passed data is not form of number  :: Adhar Id is failed for Verhoeff Algorithm Standard", true);
						}
						
					}
					
				}
					catch (Exception e) 
					{
						System.out.println(e);
					}
				
			}
					
				}

				catch (Exception e) 
		       {
					Reporter.log("Enter manually customer detail option not displayed",true);
				}
			}
}
