package scripts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class InvalidCRNAndMobileNumber 
{
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;

	@BeforeTest
	public void call() throws MalformedURLException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.login.LoginActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@Test(priority = 1)
	public void login() throws InterruptedException, IOException 
	{

		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		dr.hideKeyboard();
		loginPage.getTom_v1BTN().click();
		loginPage.getMockRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		loginPage.typeUsername(rm.getPropertyData("username"));
		dr.hideKeyboard();


		loginPage.getPasswordTB().click();
		dr.hideKeyboard();
		loginPage.typePassword(rm.getPropertyData("password"));
		loginPage.clickingSubmit();
		Thread.sleep(3000);
	}

	@Test(priority = 2,dependsOnMethods ="login")
	public void createPass() throws InterruptedException, IOException 
	{

		Thread.sleep(2000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		Reporter.log("Login successful!",true);
		try 
		{
			Alert alert = dr.switchTo().alert();
			alert.dismiss();
			


		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		for(int i=0;i<7;i++)
		{
		try 
		{
			Thread.sleep(2000);
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumber"+i));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isEnabled())
				{
					Reporter.log("Negative Test failed for test data "+rm.getPropertyData("MobileNumber"+i),true);
				}
			} catch (Exception e) {
				Reporter.log("Negative Test passed for test data "+rm.getPropertyData("MobileNumber"+i),true);
			}
		}
		catch (Exception e) 
		{
			
		}
		hp.getCRNTB().clear();
		hp.getMobileNumberTB().clear();
	
		}
	}
}
