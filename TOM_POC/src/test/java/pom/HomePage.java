package pom;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomePage {
	private AndroidDriver<AndroidElement> driver;
	public HomePage() {
    }
    public HomePage(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@index='0']")
    private AndroidElement crossMark;
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text, 'W9110')]")
    private AndroidElement EDC;
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_storevaluecrn_no")
    private AndroidElement CRNTB;
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_storevalue_mobile_num")
    private AndroidElement mobileNumberTB;
    
    @AndroidFindBy(xpath = "//android.widget.Button[@text='AUTHENTICATE']")
    private AndroidElement authenticateBTN;
    @AndroidFindBy(id = "com.paycraft.tom:id/button_storevalue_send_otp")
    private AndroidElement sendOTPBTN;
//    @AndroidFindBy(id = "com.paycraft.tom:id/textView_storevalue_manual")
//    private AndroidElement manualEntryBTN;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.ImageView")
    private AndroidElement manualEntryBTN;
    @AndroidFindBy(id = "com.paycraft.tom:id/cardview_scan_aadhar")
    private AndroidElement AdharScanBTN;
    @AndroidFindBy(id = "com.example.myapplication:id/customer_details_text")
    private AndroidElement customerDetailsText;
    @AndroidFindBy(id = "com.example.myapplication:id/store_val_text")
    private AndroidElement storeValueText;
    @AndroidFindBy(id = "com.example.myapplication:id/payment_text")
    private AndroidElement paymentText;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='TRIP PASS']")
    private AndroidElement tripPassLink;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='STORE VALUE']")
    private AndroidElement storeValueLink;
    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Basic']")
    private AndroidElement premiumRB;
    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Premium']")
    private AndroidElement basicRB;
    @AndroidFindBy(id = "com.paycraft.tom:id/button_bonded_cancel")
    private AndroidElement cancelBT;
    @AndroidFindBy(id = "com.paycraft.tom:id/imageView_bluetooth")
    private AndroidElement bluetoothIcon;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_bluetooth_device_name")
    private AndroidElement bluetoothConnectedDeviceName;
    public AndroidElement getCrossMark() 
    {
		return crossMark;
    	
    }
    public AndroidElement getAdharScanBTN() 
    {
		return AdharScanBTN;
    	
    }
    public AndroidElement getBluetoothConnectedDeviceName() 
    {
		return bluetoothConnectedDeviceName;
    	
    }
    public AndroidElement getBluetoothIcon() 
    {
		return bluetoothIcon;
    	
    }
    public AndroidElement getCancelBTN() 
    {
		return cancelBT;
    	
    }
    public AndroidElement getTripPassLink() 
    {
		return tripPassLink;
    	
    }
    public AndroidElement getStoreValueLink() 
    {
		return storeValueLink;
    	
    }
    public AndroidElement getPremiumRB() 
    {
		return premiumRB;
    	
    }
    public AndroidElement getBasicRB() 
    {
		return basicRB;
    	
    }
    public AndroidElement getSendOTPBTN() 
    {
		return sendOTPBTN;
    	
    }
    public AndroidElement getManualEntryBTN() 
    {
		return manualEntryBTN;
    	
    }
    public AndroidElement getCRNTB() 
    {
		return CRNTB;
    	
    }
    public AndroidElement getMobileNumberTB() 
    {
		return mobileNumberTB;
    	
    }
    public void typeCRN(String crn) 
    {
    	CRNTB.sendKeys(crn);
    }
    public void typeMobileNumber(String mobile) 
    {
    	mobileNumberTB.sendKeys(mobile);
    }
    public void clickingEDC() 
    {
    	EDC.click();
    }
    public void clickingSendOTPBTN() 
    {
    	sendOTPBTN.click();
    }
    public void clickingAuthenticateBTN() 
    {
    	authenticateBTN.click();
    }
    public void clickingManualEntryBTN() 
    {
    	manualEntryBTN.click();
    }
    public void clickingAdharScanBTN() 
    {
    	AdharScanBTN.click();
    }
}
