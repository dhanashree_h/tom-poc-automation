package pom;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class StoreValuePage {
	private AndroidDriver<AndroidElement> driver;
	public StoreValuePage() {
    }
    public StoreValuePage(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='₹100']")
    private AndroidElement BTN100;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='₹200']")
    private AndroidElement BTN200;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='₹300']")
    private AndroidElement BTN300;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='₹400']")
    private AndroidElement BTN400;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='₹500']")
    private AndroidElement BTN500;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='CLEAR']")
    private AndroidElement clearBTN;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_sv_amount_plus")
    private AndroidElement plusBTN;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_sv_amount_minus")
    private AndroidElement minusBTN;
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_sv_amount_value")
    private AndroidElement storeValueField;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='PAY']")
    private AndroidElement payBTN;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_sv_amount_topup_value")
    private AndroidElement eligibleTopUpAmountSet;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_sv_amount_card_fee_value")
    private AndroidElement cardFeeSet;
    
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_sv_amount_cards_value")
    private AndroidElement cardIssuedSet;
    public AndroidElement getPayBTN() {
		return payBTN;
	}
    public AndroidElement getStoreValueField() {
		return storeValueField;
	}
    public AndroidElement getMinusBTN() {
		return minusBTN;
	}
    public AndroidElement getPlusBTN() {
		return plusBTN;
	}
    public AndroidElement getClearBTN() {
		return clearBTN;
	}
    public AndroidElement getEligibleTopUpAmountSet() {
		return eligibleTopUpAmountSet;
	}
    public AndroidElement getCardFeeSet() {
		return cardFeeSet;
	}
    public AndroidElement getBTN300() {
		return BTN300;
	}
    public AndroidElement getBTN400() {
		return BTN400;
	}
    public AndroidElement getBTN100() {
		return BTN100;
	}
    public AndroidElement getBTN200() {
		return BTN200;
	}
    public AndroidElement getBTN500() {
		return BTN500;
	}
    public AndroidElement getCardIssuedSet() {
		return cardIssuedSet;
	}
    public void clickingBTN100() {
    	BTN100.click();
	}
    public void clickingBTN200() {
    	BTN200.click();
	}
    public void clickingBTN500() {
    	BTN500.click();
	}
    public void clickingBTN300() {
    	BTN300.click();
	}
    public void clickingBTN400() {
    	BTN400.click();
	}
    public void clickingClearBTN() {
    	clearBTN.click();
	}
    public void clickingPlusBTN() {
    	plusBTN.click();
	}
    public void clickingMinusBTN() {
    	minusBTN.click();
	}
    public void setStoreValueField(String amount) {
    	storeValueField.sendKeys(amount);
	}
    
    
    
}
