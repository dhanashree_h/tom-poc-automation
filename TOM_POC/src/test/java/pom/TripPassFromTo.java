package pom;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class TripPassFromTo 
{
	private AndroidDriver<AndroidElement> driver;
	public TripPassFromTo() {
    }
    public TripPassFromTo(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FROM']")
    private AndroidElement fromBTN;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Versova']")
    private AndroidElement Versova; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Marol Naka']")
    private AndroidElement Marol_Naka; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Airport Road']")
    private AndroidElement Airport_Road; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Sakinaka']")
    private AndroidElement Sakinaka; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Asalpha']")
    private AndroidElement Asalpha; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Jagruti Nagar']")
    private AndroidElement Jagruti_Nagar; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Ghatkopar']")
    private AndroidElement Ghatkopar; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Andheri']")
    private AndroidElement Andheri; 
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[2]/android.widget.FrameLayout[1]/android.widget.TextView")
    private AndroidElement firstFromStation; 
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[2]/android.widget.FrameLayout[2]/android.widget.TextView")
    private AndroidElement secondToStation; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Western Express Highway']")
    private AndroidElement Western_Express_Highway; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Chakala (J B Nagar)']")
    private AndroidElement Chakala; 
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_trip_pass_from")
    private AndroidElement fromAfterSelecting; 
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_trip_pass_to")
    private AndroidElement toAfterSelecting; 
    @AndroidFindBy(id = "com.paycraft.tom:id/errorPinMessage")
    private AndroidElement errorPinMessage; 
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.TextView[1]\n" + 
    		"")
    private AndroidElement monthPAss45; 
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.TextView[1]")
    private AndroidElement monthPAss60; 
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.widget.FrameLayout[3]/android.view.ViewGroup/android.widget.TextView[1]")
    private AndroidElement monthPAss90; 
    @AndroidFindBy(id = "android:id/message")
    private AndroidElement alertMsgAfterSelectingTripPassById; 
    @AndroidFindBy(xpath = "//android.widget.TextView[@index='1']")
    private AndroidElement alertMsgAfterSelectingTripPassByTextView; 
    @AndroidFindBy(xpath = "//android.widget.Button[@text='GO BACK']")
    private AndroidElement goBackBTN; 
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONFIRM']")
    private AndroidElement confirmBTN;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='NEXT']")
    private AndroidElement nextBTN;
    @AndroidFindBy(xpath = "//android.widget.TextView[@index='1']")
    private AndroidElement pinSetAndtripPassIssuedSuccessMsg;
    @AndroidFindBy(id = "com.paycraft.tom:id/errorMessage")
    private AndroidElement passPurchaseFailedMsg;
    @AndroidFindBy(id = "android:id/button2")
    private AndroidElement goBackBTNById; 
    @AndroidFindBy(id = "android:id/button1")
    private AndroidElement confirmBTNById;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.TextView[3]" + 
    		"")
    private AndroidElement monthPAss45amount; 
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.TextView[3]")
    private AndroidElement monthPAss60amount; 
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.widget.FrameLayout[3]/android.view.ViewGroup/android.widget.TextView[3]")
    private AndroidElement monthPAss90amount; 
	
	public AndroidElement getFromBTN() {
		return fromBTN;
	}
	public AndroidElement getNextBTN() {
		return nextBTN;
	}
	public AndroidElement getFirstFromStation() {
		return firstFromStation;
	}
	public AndroidElement getSecondToStation() {
		return secondToStation;
	}
	public AndroidElement getPassPurchaseFailedMsg() {
		return passPurchaseFailedMsg;
	}
	public AndroidElement getPinSetAndtripPassIssuedSuccessMsg() {
		return pinSetAndtripPassIssuedSuccessMsg;
	}
	public AndroidElement getErrorPinMessage() {
		return errorPinMessage;
	}
	public AndroidElement getVersova() {
		return Versova;
	}
	
	public AndroidElement getMarol_Naka() {
		return Marol_Naka;
	}
	
	public AndroidElement getAirport_Road() {
		return Airport_Road;
	}
	
	public AndroidElement getSakinaka() {
		return Sakinaka;
	}
	
	public AndroidElement getAsalpha() {
		return Asalpha;
	}
	
	public AndroidElement getJagruti_Nagar() {
		return Jagruti_Nagar;
	}
	
	public AndroidElement getGhatkopar() {
		return Ghatkopar;
	}
	
	public AndroidElement getAndheri() {
		return Andheri;
	}
	
	public AndroidElement getWestern_Express_Highway() {
		return Western_Express_Highway;
	}
	
	public AndroidElement getChakala() {
		return Chakala;
	}
	
	public AndroidElement getFromAfterSelecting() {
		return fromAfterSelecting;
	}
	
	public AndroidElement getToAfterSelecting() {
		return toAfterSelecting;
	}
	
	public AndroidElement getMonthPAss45() {
		return monthPAss45;
	}
	
	public AndroidElement getMonthPAss60() {
		return monthPAss60;
	}
	
	public AndroidElement getMonthPAss90() {
		return monthPAss90;
	}
	public AndroidElement getMonthPAss45amount() {
		return monthPAss45amount;
	}
	
	public AndroidElement getMonthPAss60amount() {
		return monthPAss60amount;
	}
	
	public AndroidElement getMonthPAss90amount() {
		return monthPAss90amount;
	}
	public AndroidElement getAlertMsgAfterSelectingTripPassById() {
		return alertMsgAfterSelectingTripPassById;
	}
	
	public AndroidElement getAlertMsgAfterSelectingTripPassByTextView() {
		return alertMsgAfterSelectingTripPassByTextView;
	}
	
	public AndroidElement getGoBackBTN() {
		return goBackBTN;
	}
	
	public AndroidElement getConfirmBTN() {
		return confirmBTN;
	}
	
	public AndroidElement getGoBackBTNById() {
		return goBackBTNById;
	}
	
	public AndroidElement getConfirmBTNById() {
		return confirmBTNById;
	}
	}
