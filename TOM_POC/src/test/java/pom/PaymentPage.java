package pom;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PaymentPage {
	private AndroidDriver<AndroidElement> driver;
	public PaymentPage() {
    }
    public PaymentPage(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cash']")
    private AndroidElement cashBTN;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Card']")
    private AndroidElement cardBTN;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Voucher']")
    private AndroidElement voucherBTN;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Wallet']")
    private AndroidElement walletBTN;
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_payment_cash_received_amount")
    private AndroidElement cashReceivedTB;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONFIRM']")
    private AndroidElement confirmBTN;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_payment_change_amount")
    private AndroidElement changeField;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_payment_care_fee_value")
    private AndroidElement cardFeeSet;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_payment_sv_value")
    private AndroidElement storeValueSet;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_payment_total_amount_value")
    private AndroidElement totalAmountSet;
    public AndroidElement getChangeField() {
		return changeField;
	}
    public AndroidElement getConfirmBTN() {
		return confirmBTN;
	}
    public AndroidElement getCashReceivedTB() {
		return cashReceivedTB;
	}
    public AndroidElement getCardFeeSet() {
		return cardFeeSet;
	}
    public AndroidElement getStoreValueSet() {
		return storeValueSet;
	}
    public AndroidElement getTotalAmountSet() {
		return totalAmountSet;
	}
    public void clickingCashBTN() {
    	cashBTN.click();
	}
    public void clickingCardBTN() {
    	cardBTN.click();
	}
    public void clickingVoucherBTN() {
    	voucherBTN.click();
	}
    public void clickingWalletBTN() {
    	walletBTN.click();
	}
    public void clickingCashReceivedTB() {
    	cashReceivedTB.click();
	}
    public void clickingConfirmBTN() {
    	confirmBTN.click();
	}
    public void setCashReceived(String amount) {
    	cashReceivedTB.sendKeys(amount);
	}
    public String getChangeFieldValue() {
		return changeField.getText();
    	
	}
    
    
}
