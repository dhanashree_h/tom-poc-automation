package pom;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CustomerDetailsManually {
	private AndroidDriver<AndroidElement> driver;
    public  CustomerDetailsManually() {
    }
    public CustomerDetailsManually(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
  
   
    //Dropdown arrow
    @AndroidFindBy(id = "com.paycraft.tom:id/spinner_manual_subtitle")
    private AndroidElement dropDownArrow;
   
    //select Mrs
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Mrs']")
    private AndroidElement firstNameDropDownOption;
    
    //Enter FirstName
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_manual_first_name")
    private AndroidElement firstNameTB;
    
    //Enter LastName
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_manual_last_name")
    private AndroidElement lastNameTB;
    
    //Enter Dob
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_manual_dob")
    private AndroidElement dobTB;
    
    //Enter DropdownGender
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Male']")
    private AndroidElement dropDownMaleOption;
    
    //Enter Female
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Female']")
    private AndroidElement dropDownFemaleOption;
    
    //Clcik on radio button
    @AndroidFindBy(id = "com.paycraft.tom:id/radio_button_manual_aadhaar")
    private AndroidElement adharradioBtn;
    
    
    @AndroidFindBy(id = "com.paycraft.tom:id/radio_button_manual_pan")
    private AndroidElement panradioBtn;
    
    @AndroidFindBy(id = "com.paycraft.tom:id/radio_button_manual_voterid")
    private AndroidElement voterradioBtn;
    
    
    @AndroidFindBy(id = "com.paycraft.tom:id/radio_button_manual_mnrega")
    private AndroidElement mnregaradioBtn;
    
    @AndroidFindBy(id = "com.paycraft.tom:id/radio_button_manual_passport")
    private AndroidElement passportradioBtn;
    
    @AndroidFindBy(id = "com.paycraft.tom:id/radio_button_manual_drive")
    private AndroidElement driveradioBtn;
    //Enter IdNum
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_manual_id")
    private AndroidElement idNumTB;
    
    //Enter Otp
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_manual_otp")
    private AndroidElement OTPTB;
    
    //Enter resend
    @AndroidFindBy(id = "com.example.myapplication:id/resendOtp")
    private AndroidElement resendOTPTB;
    
    //Enter VerifyBtn
    @AndroidFindBy(id = "com.example.myapplication:id/actionThree")
    private AndroidElement verifyBTN;
    
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_manual_timer")
    private AndroidElement resendOTPBTN;
    
    //Select Gender
    @AndroidFindBy(id = "com.paycraft.tom:id/spinner_manual_gender")
    private AndroidElement selectGender;
    
    //select female Optin
    @AndroidFindBy(xpath = "//android.widget.TextView[@index='1']")
    private AndroidElement selectFemale;
    
    @AndroidFindBy(id = "com.paycraft.tom:id/button_manual_entry_confirm_otp")
    private AndroidElement confirmOTPBTN;
    
	public AndroidElement getDropDownArrow() {
		return dropDownArrow;
	}
	public AndroidElement getResendOTPBTN() {
		return resendOTPBTN;
	}
	public AndroidElement getConfirmOTPBTN() {
		return confirmOTPBTN;
	}
	public AndroidElement getFirstNameDropDownOption() {
		return firstNameDropDownOption;
	}
	
	public AndroidElement getFirstNameTB() {
		return firstNameTB;
	}
	public void setFirstNameTB(String firstName) {
		firstNameTB.sendKeys(firstName);
	}
	public AndroidElement getLastNameTB() {
		return lastNameTB;
	}
	public void setLastNameTB(String lastName) {
		lastNameTB.sendKeys(lastName);
	}
	public AndroidElement getDobTB() {
		return dobTB;
	}
	public void setDobTB(String dob) {
		dobTB.sendKeys(dob);
	}
	public AndroidElement getDropDownMaleOption() {
		return dropDownMaleOption;
	}
	public void clickingDropDownMaleOption() {
		dropDownMaleOption.click();
	}
	public AndroidElement getDropDownFemaleOption() {
		return dropDownFemaleOption;
	}
	public void clickingDropDownFemaleOption() {
	dropDownFemaleOption.click();
	}
	public AndroidElement getRadioBtn() {
		return adharradioBtn;
	}
	public AndroidElement getpanradioBtn() {
		return panradioBtn;
	}
	public AndroidElement getvoterradioBtn() {
		return voterradioBtn;
	}
	public AndroidElement getmnregaradioBtn() {
		return mnregaradioBtn;
	}
	public AndroidElement getpassportradioBtn() {
		return passportradioBtn;
	}
	public AndroidElement getdriveradioBtn() {
		return adharradioBtn;
	}
	public void clickingRadioBtn() {
		adharradioBtn.click();
	}
	public AndroidElement getIdNumTB() {
		return idNumTB;
	}
	public void setIdNumTB(String idnum) {
		idNumTB.sendKeys(idnum);
	}
	public AndroidElement getOTPTB() {
		return OTPTB;
	}
	public void setOTPTB(String otp) {
		OTPTB.sendKeys(otp);;
	}
	public AndroidElement getResendOTPTB() {
		return resendOTPTB;
	}
	public void clickingResendOTPTB() {
		resendOTPTB.click();
	}
	public AndroidElement getVerifyBTN() {
		return verifyBTN;
	}
	public void clickingVerifyBTN() {
		verifyBTN.click();
	}
    public void clickingselectGender() {
	   selectGender.click();
	}
    
    public void selectFema() {
    	selectFemale.click();
		}
    
	        
	        
			
	        
	        
	             


	}
	 







