package baseLibrary;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;

public class ReusableMethods 
{
	static String scrShotDir = "screenshots";
	File scrFile;
	static File scrShotDirPath = new java.io.File("./"+ scrShotDir+ "//");
	static String destFile;
	   public String getPropertyData(String key) throws IOException {
		   FileReader reader=new FileReader("./src/main/resources/Data.property");  
		      
		    Properties p=new Properties();  
		    p.load(reader);
			return p.getProperty(key,"key not found");  
		      
		
	}
	   public String readExcel(String sheet,int row,int cell) throws EncryptedDocumentException, IOException {
			
			
			FileInputStream fis = new FileInputStream("./src/main/resources/exel.xlsx");
			// create object for work book
			Workbook wb = WorkbookFactory.create(fis);
			//create object for sheet present in excel using Workbook object 'wb'
			Sheet sheetname = wb.getSheet(sheet);
			//create object for row present in sheet using Sheet object 'sheet'
			Row rowname = sheetname.getRow(row);
			//create object for cell present in row using Row object 'row'
			Cell cellname = rowname.getCell(cell);
			//print the value present in the excel sheet
			
				return cellname.getStringCellValue();
			
			
			
			

	}
	  
	   public int getRowCount(String sheet) throws EncryptedDocumentException, IOException {
			
			
			FileInputStream fis = new FileInputStream("./src/main/resources/exel.xlsx");
			// create object for work book
			Workbook wb = WorkbookFactory.create(fis);
			//create object for sheet present in excel using Workbook object 'wb'
			Sheet sheetname = wb.getSheet(sheet);
			int rowcount=sheetname.getLastRowNum();
			
			return rowcount;
			

	}
	   public static void takeScreenShot(AndroidDriver driver) {
			TakesScreenshot ts=(TakesScreenshot)driver;
			  File scrFile = ts.getScreenshotAs(OutputType.FILE); 
			  
			  SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			  new File(scrShotDir).mkdirs(); // Create folder under project with name
			          // "screenshots" if doesn't exist
			  destFile = dateFormat.format(new Date()) + ".png"; // Set file name
			               // using current
			               // date time.
			  try {
			   FileUtils.copyFile(scrFile, new File(scrShotDir + "/" + destFile)); // Copy
			                    // paste
			                    // file
			                    // at
			                    // destination
			                    // folder
			                    // location
			  } catch (IOException e) {
			   System.out.println("Image not transfered to screenshot folder");
			   e.printStackTrace();
			  }
			  
			 }
      
	      
}
