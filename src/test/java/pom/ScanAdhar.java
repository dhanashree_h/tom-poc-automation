package pom;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ScanAdhar {
	private AndroidDriver<AndroidElement> driver;
    public  ScanAdhar() {
    }
    public ScanAdhar(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    //Enter FirstName
    @AndroidFindBy(id = "com.paycraft.tom:id/appCompatEditText_cust_first_name")
    private AndroidElement firstNameTB;
    
    //Enter LastName
    @AndroidFindBy(id = "com.paycraft.tom:id/appCompatEditText_cust_last_name")
    private AndroidElement lastNameTB;
    
    //Enter Dob
    @AndroidFindBy(id = "com.paycraft.tom:id/appCompatEditText_cust_dob")
    private AndroidElement dobTB;
    
    //Enter DropdownGender
    @AndroidFindBy(id = "com.paycraft.tom:id/spinner_manual_subtitle2")
    private AndroidElement dropDownSalutation;
    
    //Mrs salutation
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Mrs']")
    private AndroidElement MrsSalutation;
    //Enter Female
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Female']")
    private AndroidElement dropDownFemaleOption;
    
  //Enter genderDD
    @AndroidFindBy(id = "com.paycraft.tom:id/spinner_cust_gender")
    private AndroidElement genderDD;
    
    //Enter aadhar
    @AndroidFindBy(id = "com.paycraft.tom:id/appCompatEditText_cust_id_num")
    private AndroidElement adharIDTB;
    
    @AndroidFindBy(id = "com.paycraft.tom:id/appCompactEditText_cust_otp")
    private AndroidElement OTPTB;
    
    @AndroidFindBy(id = "com.paycraft.tom:id/button_cust_validate_otp")
    private AndroidElement verifyOTPBTN;
	public AndroidElement getFirstNameTB() {
		return firstNameTB;
	}
	public void setFirstNameTB(String firstname) {
		firstNameTB.sendKeys(firstname);
	}
	public AndroidElement getLastNameTB() {
		return lastNameTB;
	}
	public void setLastNameTB(String lastname) {
		lastNameTB.sendKeys(lastname);
	}
	public AndroidElement getDobTB() {
		return dobTB;
	}
	public void setDobTB(String dob) {
		dobTB.sendKeys(dob);
	}
	public AndroidElement getDropDownSalutation() {
		return dropDownSalutation;
	}
	public void clickDropDownSalutation() {
		dropDownSalutation.click();
	}
	public AndroidElement getMrsSalutation() {
		return MrsSalutation;
	}
	public void clickMrsSalutation() {
		MrsSalutation.click();
	}
	public AndroidElement getDropDownFemaleOption() {
		return dropDownFemaleOption;
	}
	public void clickDropDownFemaleOption() {
	dropDownFemaleOption.click();
	}
	public AndroidElement getGenderDD() {
		return genderDD;
	}
	public void clickGenderDD() {
		genderDD.click();
	}
	public AndroidElement getAdharIDTB() {
		return adharIDTB;
	}
	public void setAdharIDTB(String adhar) {
		adharIDTB.sendKeys(adhar);
	}
	public AndroidElement getOTPTB() {
		return OTPTB;
	}
	public AndroidElement getverifyOTPBTN() {
		return verifyOTPBTN;
	}
}
