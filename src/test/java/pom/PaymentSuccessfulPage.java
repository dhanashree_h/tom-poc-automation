package pom;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PaymentSuccessfulPage {
	private AndroidDriver<AndroidElement> driver;
	public PaymentSuccessfulPage() {
    }
    public PaymentSuccessfulPage(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
//    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Payment received succesfully!']")
//    private AndroidElement paymentReceivedSuccessfullyMsg;
//    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Card issued and linked succesfully!']")
//    private AndroidElement cardIssuedAndLinkedSuccessfullyMsg;
//    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Host top up done succesfully!']")
//    private AndroidElement hostTopUpDoneSuccessfullyMsg;
    @AndroidFindBy(xpath = "//android.widget.TextView[@index='1']")
    private AndroidElement paymentReceivedSuccessfullyMsg;
    @AndroidFindBy(xpath = "//android.widget.TextView[@index='2']")
    private AndroidElement cardIssuedAndLinkedSuccessfullyMsg;
    @AndroidFindBy(xpath = "//android.widget.TextView[@index='3']")
    private AndroidElement hostTopUpDoneSuccessfullyMsg;
    @AndroidFindBy(id = "com.paycraft.tom:id/textView_cardBalance")
    private AndroidElement cardBalance;
//    @AndroidFindBy(id = "com.paycraft.tom:id/button_go_to_home_screen")
//    private AndroidElement confirmBTN;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONFIRM']")
    private AndroidElement confirmBTN;
    public AndroidElement getPaymentReceivedSuccessfullyMsg() {
		return paymentReceivedSuccessfullyMsg;
	}
    public AndroidElement getCardIssuedAndLinkedSuccessfullyMsg() {
		return cardIssuedAndLinkedSuccessfullyMsg;
	}
    public AndroidElement getHostTopUpDoneSuccessfullyMsg() {
		return hostTopUpDoneSuccessfullyMsg;
	}
    public AndroidElement getCardBalance() {
		return cardBalance;
	}
    public AndroidElement getconfirmBTN() {
		return confirmBTN;
	}
}
