package pom;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginPage {
	private AndroidDriver<AndroidElement> driver;
	public LoginPage() {
    }
    public LoginPage(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_login_username")
    private AndroidElement usernameTB;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Required']")
    private AndroidElement requiredErrorMsg;
    @AndroidFindBy(id = "com.paycraft.tom:id/editText_login_password")
    private AndroidElement passwordTB;
    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Password']")
    private AndroidElement passwordImageBtn;
    @AndroidFindBy(id = "com.paycraft.tom:id/button_login")
    private AndroidElement submitBTN;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Tom_V1']")
    private AndroidElement tom_v1BTN;
    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Mock']")
    private AndroidElement mockRadioBtn;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='Proceed']")
    private AndroidElement proceedBtn;
    @AndroidFindBy(id = "com.paycraft.tom:id/radioButton_mock")
    private AndroidElement MockRadioBtn;
    @AndroidFindBy(id = "com.paycraft.tom:id/radioButton_cms")
    private AndroidElement CMSRadioBtn;
    public AndroidElement getMockoRadioBtn() {
		return MockRadioBtn;
    	
    }
    public AndroidElement getCMSRadioBtn() {
		return CMSRadioBtn;
    	
    }
    public AndroidElement getPasswordTB() {
		return passwordTB;
    	
    }
    public AndroidElement getTom_v1BTN() {
		return tom_v1BTN;
    	
    }
    public AndroidElement getMockRadioBtn() {
		return mockRadioBtn;
    	
    }
    public AndroidElement getProceedBtn() {
		return proceedBtn;
    	
    }
    public AndroidElement getrequiredErrorMsg() {
		return requiredErrorMsg;
    	
    }
    public AndroidElement getusernameTB() {
		return usernameTB;
    	
    }
    public AndroidElement getpasswordImageBtn() {
		return passwordImageBtn;
    	
    }
    public void typeUsername(String username) {
    	usernameTB.sendKeys(username);
    }
    public void typePassword(String password) {
    	passwordTB.sendKeys(password);
    }
    public void clickingSubmit() {
    	submitBTN.click();
    }
}
