package scripts;

import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class UserStoryAM110 
{
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;

	@Test
	public void call() throws MalformedURLException, InterruptedException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.splash.SplashActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		loginPage.getMockoRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		Reporter.log("Login successful!",true);
		Reporter.log("----------User Story AM-110----------", true);
		try 
		{
			hp.getBluetoothIcon().click();
			hp.clickingEDC();
			try
			{
			hp.getCancelBTN().click();
			Reporter.log("Connected device name : "+hp.getBluetoothConnectedDeviceName().getText(), true);
			
			}
			catch (NoSuchElementException e) {
				Reporter.log("Exception occure while clicking on cancel button of bluetooth ",true);
			}

		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		try 
		{
			Thread.sleep(2000);
			//hp.typeCRN(rm.getPropertyData("CRN"));
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		
		

		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Enter manually customer detail option displayed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("enter customer detail manualy page displayed",true);
					cdm.getDropDownArrow().click();
					cdm.getFirstNameDropDownOption().click();
					cdm.setFirstNameTB("dhanashree");
					dr.hideKeyboard();
					cdm.setLastNameTB("hegde");
					dr.hideKeyboard();
					cdm.getRadioBtn().click();
					cdm.getRadioBtn().click();
					cdm.setDobTB("12/02/1993");
					dr.hideKeyboard();
					cdm.clickingselectGender();
					cdm.selectFema();
					cdm.setIdNumTB("452131595950");
					Thread.sleep(1000);

//					dr.hideKeyboard();
					
					cdm.setOTPTB("1234");
					Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-110",1 ,2 )+" Passed------>",true);
					Reporter.log("Entered OTP is: "+cdm.getOTPTB().getText().replace(", Enter OTP", ""),true);
					dr.findElementByXPath("//android.widget.TextView[@text='Resend OTP']").click();
					try
					{
						MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
						String resendotptoast=dr.findElementByXPath("//android.widget.Toast[1]").getText();
						if(resendotptoast.equalsIgnoreCase("OTP has been resent"))
						{
							Reporter.log("Exception Criteria----------"+rm.readExcel("AM-110",2 ,4 )+" Passed ---->",true);
						}
					}
					catch (NoSuchElementException e) 
					  {
						
						}

					
					dr.hideKeyboard();
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							cdm.getConfirmOTPBTN().click();
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-110",2 ,2 )+" Passed------>",true);
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-110",3 ,2 )+" Passed------>",true);
							Reporter.log("Clicked on Verify Button ",true);
							try 
							{
								
								MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
								String otptoast=dr.findElementByXPath("//android.widget.Toast[1]").getText();
								if(otptoast.equalsIgnoreCase("invalid otp"))
								{
									Reporter.log("Invalid OTP ",true);
									Reporter.log("Exception Criteria----------"+rm.readExcel("AM-110",3 ,4 )+" Passed ---->",true);
								}
								else if(otptoast.equalsIgnoreCase("OTP verification Successful"))
								{
									Reporter.log("Valid OTP ",true);
									Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-110",4 ,2 )+" Passed------>",true);
									stp=new StoreValuePage(dr);
									Thread.sleep(2000);
									try 
									{
										if(stp.getPayBTN().isDisplayed())
										{
										Reporter.log("store value page displayed",true);
										Reporter.log("Card issued displayed as: "+stp.getCardIssuedSet().getText(),true);
									    Reporter.log("Eligible top up amounts displayed as: "+stp.getEligibleTopUpAmountSet().getText(),true);
									    
										}
										
									
									
									}
									catch(Exception e) 
									{
										Reporter.log("Exception in store value page ",true);
									}
								}
								Reporter.log("Server response/Toast message is: "+otptoast,true);
								rm.takeScreenShot(dr);
							}
							catch (NoSuchElementException ex) 
							{
								Reporter.log("toast not displayed",true);
							}
							Thread.sleep(2000);
						}
						else
						{
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-110",2 ,2 )+" Failed ---->",true);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on Verify otp button",true);
					}
				}

				catch (Exception e) 
				{
					Reporter.log("Exception occured while entering customer detail manualy ",true);
				}
					
					
					
			}
		}
		catch (Exception e) {
			Reporter.log("Enter manually customer detail option not displayed",true);
		}
		
		

	}


}
