package scripts;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class Login {
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;

	@BeforeTest
	public void call() throws MalformedURLException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.login.LoginActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

	}

	@Test(priority = 1)
	public void login() throws InterruptedException, IOException 
	{

		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		dr.hideKeyboard();
		loginPage.getTom_v1BTN().click();
		loginPage.getMockRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		loginPage.typeUsername(rm.getPropertyData("username"));
		dr.hideKeyboard();


		loginPage.getPasswordTB().click();
		dr.hideKeyboard();
		loginPage.typePassword(rm.getPropertyData("password"));
		loginPage.clickingSubmit();
		Thread.sleep(3000);
	}

	@Test(priority = 2,dependsOnMethods ="login")
	public void createPass() throws InterruptedException, IOException 
	{

		Thread.sleep(2000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		Reporter.log("Login successful!",true);
		try 
		{
			Alert alert = dr.switchTo().alert();
			alert.dismiss();
			


		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		try 
		{
			Thread.sleep(2000);
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumber"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		//	try 
		//	{
		//		if(hp.getCrossMark().isDisplayed())
		//		{
		//			System.out.println("Input session id not provided");
		//			hp.clickingAuthenticateBTN();
		//			try 
		//			  {
		//				Thread.sleep(10000);
		//				  MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
		//					System.out.println("Server response/Toast message is: "+toast.getText());
		//					
		//					rm.takeScreenShot(dr);
		//			  }
		//			  catch (NoSuchElementException ex) {
		//				System.out.println("toast not displayed");
		//			}
		//			Thread.sleep(8000);
		//			hp.clickingSendOTPBTN();
		//			try 
		//			  {
		//				Thread.sleep(1000);
		//				  MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
		//					System.out.println("Server response/Toast message is: "+toast.getText());
		//					
		//					rm.takeScreenShot(dr);
		//			  }
		//			  catch (NoSuchElementException ex) {
		//				System.out.println("");
		//			}
		//			
		//		}
		//	} 

		//	catch (NoSuchElementException e) {
		//		System.out.println("CRN and mobile number entered and clicked on send OTP button");
		//	}


		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Enter manually customer detail option displayed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("enter customer detail manualy page displayed",true);
					cdm.getDropDownArrow().click();
					cdm.getFirstNameDropDownOption().click();
					cdm.setFirstNameTB("dhanashree");
					dr.hideKeyboard();
					cdm.setLastNameTB("hegde");
					dr.hideKeyboard();
					cdm.getRadioBtn().click();
					cdm.getRadioBtn().click();
					cdm.setIdNumTB("452131595950");
					try 
					{
						
						MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
						Reporter.log("Server response/Toast message is: "+toast.getText(),true);

						rm.takeScreenShot(dr);
					}
					catch (NoSuchElementException ex) 
					{
						Reporter.log("toast not displayed",true);
					}
					dr.hideKeyboard();
					cdm.setDobTB("12/02/1993");
					dr.hideKeyboard();
					


					
					cdm.setOTPTB("1234");
					dr.hideKeyboard();
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							cdm.getConfirmOTPBTN().click();
							Thread.sleep(2000);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on confirm otp button",true);
					}
				}

				catch (Exception e) 
				{
					Reporter.log("enter customer detail manualy page not displayed",true);
				}
					//cdm.clickingVerifyBTN();
					stp=new StoreValuePage(dr);
					Thread.sleep(2000);
					try 
					{
						
							
							stp.clickingBTN100();
							stp.clickingClearBTN();
							stp.setStoreValueField("300");
							dr.hideKeyboard();
							Reporter.log("store value page displayed",true);
							try 
							{
								stp.getPayBTN().click();
							} 
							catch (Exception e) 
							{
								Reporter.log("set amount first",true);
								try 
								{
									stp.getPayBTN().click();
								}
								catch (Exception ex) 
								{
									Reporter.log("pay button not displayed",true);
								}
							}
						
					}
					catch(Exception e) 
					{
						Reporter.log("store value page not displayed",true);
					}

					pp=new PaymentPage(dr);
					try {
						if(pp.getConfirmBTN().isDisplayed())
						{
							Reporter.log("payment page displayed",true);
							pp.clickingCashBTN();
							pp.setCashReceived("100");
							Thread.sleep(3000);
							Reporter.log("change to be return "+ pp.getChangeField().getText(),true);
							int amount=Integer.parseInt(pp.getChangeField().getText());
							if(amount<0)
							{
								Thread.sleep(2000);
								Reporter.log("Cash received is less than total amount please enter amount more than or equal to total amount ",true);
								Thread.sleep(2000);
								pp.setCashReceived("");
								pp.setCashReceived("600");
								Reporter.log("Now change to be return "+ pp.getChangeField().getText(),true);
								
							}
							pp.clickingConfirmBTN();
							Thread.sleep(2000);
							psp=new PaymentSuccessfulPage(dr);
							try
							{
								if(psp.getPaymentReceivedSuccessfullyMsg().isDisplayed()&&psp.getCardIssuedAndLinkedSuccessfullyMsg().isDisplayed()&&psp.getHostTopUpDoneSuccessfullyMsg().isDisplayed())
								{
									Reporter.log("Payment Received Successfully message displayed",true);
									Reporter.log("Card Issued and Linked Successfully message displayed",true);
									Reporter.log("Host TOP Up Successfully message displayed",true);
								}
							
							}
							catch (NoSuchElementException e)
							{
								Reporter.log("Payment successful message not displayed",true);
							
							try 
							{
								Thread.sleep(10000);
								MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
								Reporter.log("Server response/Toast message is: "+toast.getText(),true);

								rm.takeScreenShot(dr);
							}
							catch (NoSuchElementException ex) 
							{
								Reporter.log("toast not displayed",true);
							}
						}
						}
					} 
					catch (Exception e) {
						Reporter.log("payment page not displayed",true);
					}


				

			}
		}
		catch (NoSuchElementException e) 
		{
			Reporter.log("enter customer detail manualy option not displayed",true);
			try 
			{
				Thread.sleep(10000);
				MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
				Reporter.log("Server response/Toast message is: "+toast.getText(),true);

				rm.takeScreenShot(dr);
			}
			catch (NoSuchElementException ex) 
			{
				Reporter.log("toast not displayed",true);
			}
		}
	}
}








