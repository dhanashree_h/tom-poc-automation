package scripts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.ScanAdhar;
import pom.StoreValuePage;

public class UserStoryAM104 
{
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	ScanAdhar sa;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;
	

	@Test
	public void call() throws InterruptedException, EncryptedDocumentException, IOException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.splash.SplashActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		loginPage.getMockoRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		Reporter.log("Login successful!",true);
		Reporter.log("----------User Story AM-104----------", true);
		try 
		{
			hp.getBluetoothIcon().click();
			//hp.clickingEDC();
		dr.findElementByXPath("//android.widget.TextView[contains(@text, 'Redmi')]").click();
			try
			{
			hp.getCancelBTN().click();
			Reporter.log("Connected device name : "+hp.getBluetoothConnectedDeviceName().getText(), true);
			
			}
			catch (NoSuchElementException e) {
				Reporter.log("Exception occure while clicking on cancel button of bluetooth ",true);
			}

		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		try 
		{
			Thread.sleep(2000);
			//hp.typeCRN(rm.getPropertyData("CRN"));
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		
		

		try {
			
			if(hp.getAdharScanBTN().isDisplayed())
			{
				
				sa=new ScanAdhar(dr);
				cdm=new CustomerDetailsManually(dr);
				hp.clickingAdharScanBTN();
				
				Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",1 , 2)+" Passed------>",true);
				try 
				{
					Thread.sleep(25000);
					int count=0;
					if(sa.getFirstNameTB().getText().equals("First Name"))
							{
						count++;
						sa.getFirstNameTB().sendKeys("dhanu");
						sa.getDropDownSalutation().click();
						sa.getMrsSalutation().click();
						sa.getGenderDD().click();
						sa.getDropDownFemaleOption().click();
						Reporter.log("Exception Criteria "+rm.readExcel("AM-104",3 ,4 )+" Passed------>First Name entered manually",true);
						Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",2 , 2)+" Failed------>First Name NOT Auto Populated",true);
						
							}
					if(sa.getLastNameTB().getText().equals("Last Name"))
					{
						count++;
				sa.getLastNameTB().sendKeys("hegde");
				Reporter.log("Exception Criteria "+rm.readExcel("AM-104",3 ,4 )+" Passed------>Last Name entered manually",true);
				Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",2 , 2)+" Failed------>Last Name NOT Auto Populated",true);
				
					}
					if(sa.getDobTB().getText().equals("DOB"))
					{
						count++;
				sa.getDobTB().sendKeys("12/02/1993");
				Reporter.log("Exception Criteria "+rm.readExcel("AM-104",3 ,4 )+" Passed------>DOB entered manually",true);
				Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",2 , 2)+" Failed------>DOB NOT Auto Populated",true);
				
					}
					if(sa.getAdharIDTB().getText().equals("ID number"))
					{
						count++;
				sa.getAdharIDTB().sendKeys("452131595950");
				Reporter.log("Exception Criteria "+rm.readExcel("AM-104",3 ,4 )+" Passed------>Aadhar ID entered manually",true);
				Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",2 , 2)+" Failed------>Aadhar ID NOT Auto Populated",true);
				
					}
					sa.getOTPTB().sendKeys("1234");
					try {
						if(sa.getverifyOTPBTN().isEnabled())
						{
							sa.getverifyOTPBTN().click();
							if(count==0)
							{
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",2 , 2)+" Passed------>",true);
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",3 , 2)+" Passed------>",true);
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",4 , 2)+" Passed------>",true);
							}
							else
							{
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",3 , 2)+" Passed------>",true);
								Reporter.log("Acceptance Criteria "+rm.readExcel("AM-104",4 , 2)+" Passed------>",true);
							}
							try 
							{
								
								MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
								Reporter.log("Server response/Toast message is: "+toast.getText(),true);
		
								rm.takeScreenShot(dr);
							}
							catch (NoSuchElementException ex) 
							{
								
							}
							Thread.sleep(2000);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on Verify button",true);
					}

					
				}
				catch (NoSuchElementException e) 
				{
					try
					{
					MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
					String toastmsg=dr.findElementByXPath("//android.widget.Toast[1]").getText();
					if(toastmsg.equalsIgnoreCase("EDC not connected"))
					{
						Reporter.log("Server response/Toast message is: "+toastmsg,true);
						Reporter.log("Exception Criteria "+rm.readExcel("AM-104",1 ,4 )+" Passed------>",true);
						dr.findElementByXPath("//android.widget.TextView[contains(@text, 'Redmi')]").click();
					}
					}
					catch (Exception ex) {
						
					}
					
				}
				
			}
		} 
			
		catch (Exception e) 
		{
			Reporter.log("Exception while clicking Scan Adhar option"+e,true);
			
		}
		
		

	}

}
