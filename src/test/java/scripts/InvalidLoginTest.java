package scripts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import baseLibrary.VerhoeffAlgorithm;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class InvalidLoginTest {
	
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;
	VerhoeffAlgorithm va;
	

	@Test
	public void call() throws EncryptedDocumentException, IOException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.login.LoginActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		rm=new ReusableMethods();
		va=new VerhoeffAlgorithm();
		System.out.println(va.validateVerhoeff("452131595950"));
	}
	
	}
	

