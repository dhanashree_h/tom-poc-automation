package scripts;

import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class UserStoryAM293 
{
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;

	@Test
	public void call() throws MalformedURLException, InterruptedException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.splash.SplashActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		loginPage.getMockoRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		Reporter.log("Login successful!",true);
		Reporter.log("----------User Story AM-293----------", true);
		try 
		{
			hp.getBluetoothIcon().click();
			hp.clickingEDC();
			try
			{
			hp.getCancelBTN().click();
			Reporter.log("Connected device name : "+hp.getBluetoothConnectedDeviceName().getText(), true);
			
			}
			catch (NoSuchElementException e) {
				Reporter.log("Exception occure while clicking on cancel button of bluetooth ",true);
			}

		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		try 
		{
			Thread.sleep(2000);
			//hp.typeCRN(rm.getPropertyData("CRN"));
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		
		

		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Acceptance criteria-----"+rm.readExcel("AM-293", 1, 2)+" Passed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("Acceptance criteria-----"+rm.readExcel("AM-293", 2, 2)+" Passed",true);
					cdm.getDropDownArrow().click();
					cdm.getFirstNameDropDownOption().click();
					cdm.setFirstNameTB("");
					dr.hideKeyboard();
					cdm.setLastNameTB("hegde");
					dr.hideKeyboard();
					cdm.getRadioBtn().click();
					cdm.getRadioBtn().click();
					cdm.setDobTB("12/02/1993");
					dr.hideKeyboard();
					cdm.clickingselectGender();
					cdm.selectFema();
					cdm.setIdNumTB("452131595950");
					Thread.sleep(1000);

//					dr.hideKeyboard();
					
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							
							Reporter.log("Exception criteria---------- "+rm.readExcel("AM-293", 1, 4)+" Failed",true);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("Exception criteria---------- "+rm.readExcel("AM-293",1 , 4)+" Passed",true);
					}
					cdm.setFirstNameTB("akshay");
					dr.hideKeyboard();
					cdm.setOTPTB("1235");
					dr.hideKeyboard();
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							cdm.getConfirmOTPBTN().click();
							try 
							{
								
								MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
								Reporter.log("Server response/Toast message is: "+toast.getText(),true);
		                        if(toast.getText().equalsIgnoreCase("invalid OTP"))
		                        {
		                        	Reporter.log("Exception criteria---------- "+rm.readExcel("AM-293",2 , 4)+" Passed",true);
		                        }
		                        else
		                        {
		                        	Reporter.log("Exception criteria---------- "+rm.readExcel("AM-293",2 , 4)+" Failed",true);
		                        }
								
							}
							catch (NoSuchElementException ex) 
							{
								Reporter.log("",true);
							}
							Thread.sleep(2000);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on Verify button",true);
					}
					cdm.getResendOTPBTN().click();
					Thread.sleep(500);
						try 
						{
							
							MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
							Reporter.log("Server response/Toast message is: "+toast.getText(),true);
	                        if(toast.getText().equalsIgnoreCase("OTP has been resent"))
	                        {
	                        	Reporter.log("Exception criteria---------- "+rm.readExcel("AM-293",3, 4)+" Passed",true);
	                        }
	                        else
	                        {
	                        	Reporter.log("Exception criteria---------- "+rm.readExcel("AM-293",3, 4)+" Failed",true);
	                        }
							
					}
					catch (Exception e) 
						{
						Reporter.log("Exception criteria---------- "+rm.readExcel("AM-293",3, 4)+" Failed",true);
					    }
						cdm.getOTPTB().clear();
						cdm.setOTPTB("1234");
						dr.hideKeyboard();
						Reporter.log("Acceptance criteria-----"+rm.readExcel("AM-293", 3, 2)+" Passed",true);
						try
						{
						Reporter.log("Official valid document displayed as: "+cdm.getRadioBtn().getText()+" "+cdm.getpanradioBtn().getText()+" "+cdm.getmnregaradioBtn().getText()+" "+cdm.getdriveradioBtn().getText()+" "+cdm.getvoterradioBtn().getText()+" "+cdm.getpassportradioBtn().getText());
						Reporter.log("Acceptance criteria-----"+rm.readExcel("AM-293",11, 2)+" Passed",true);
						}
						catch (NoSuchElementException e) {
							Reporter.log("Acceptance criteria-----"+rm.readExcel("AM-293", 11, 2)+" Failed",true);
						}
						
						try {
							if(cdm.getConfirmOTPBTN().isEnabled())
							{
								
								
								Reporter.log("Acceptance criteria-----"+rm.readExcel("AM-293", 12, 2)+" Passed",true);
							}
						} 
						catch (Exception e) 
						{
							Reporter.log("Acceptance criteria-----"+rm.readExcel("AM-293", 12, 2)+" Failed",true);
						}
				}
            
				catch (Exception e) 
				{
					Reporter.log("Exception occured while entering customer detail manualy ",true);
				}
			}
		}
		catch (Exception e) {
			Reporter.log("Enter manually customer detail option not displayed",true);
		}
		
	}

}
