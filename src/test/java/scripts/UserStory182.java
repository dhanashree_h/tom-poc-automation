package scripts;

import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;
import pom.TripPassFromTo;

public class UserStory182 
{
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;
	TripPassFromTo tpft;

	@Test
	public void call() throws MalformedURLException, InterruptedException 
	{
		DesiredCapabilities cap=new DesiredCapabilities();
		System.setProperty("webdriver.http.factory", "apache");
		cap=new DesiredCapabilities();
		cap.setCapability("platformName","Android");
		cap.setCapability("platformVersion","7");
		cap.setCapability("appPackage","com.paycraft.tom");
		cap.setCapability("appActivity","com.paycraft.tom.splash.SplashActivity");
		cap.setCapability("deviceName","T2mini");
		cap.setCapability("automationName","Appium");
		cap.setCapability("UDID","TN04197E40003");
		//cap.setCapability("autoGrantPermissions",true);
		dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
		dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		LoginPage loginPage = new LoginPage(dr);
		rm=new ReusableMethods();
		loginPage.getMockoRadioBtn().click();
		loginPage.getProceedBtn().click();
		Thread.sleep(1000);
		hp=new HomePage(dr);
		rm=new ReusableMethods();
		Reporter.log("Login successful!",true);
		Reporter.log("User Story AM-182", true);
		try 
		{
			hp.getBluetoothIcon().click();
			//hp.clickingEDC();
			dr.findElementByXPath("//android.widget.TextView[contains(@text, 'Redmi')]").click();
			try
			{
			hp.getCancelBTN().click();
			Reporter.log("Connected device name : "+hp.getBluetoothConnectedDeviceName().getText(), true);
			
			}
			catch (NoSuchElementException e) {
				Reporter.log("Exception occure while clicking on cancel button of bluetooth ",true);
			}

		}
		catch (NoAlertPresentException e) 
		{
			Reporter.log("Bluetooth connection alert not displayed",true);
		}
		try 
		{
			Thread.sleep(2000);
			hp.getTripPassLink().click();
			//hp.typeCRN(rm.getPropertyData("CRN"));
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		
		

		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Enter manually customer detail option displayed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("enter customer detail manualy page displayed",true);
					cdm.getDropDownArrow().click();
					cdm.getFirstNameDropDownOption().click();
					cdm.setFirstNameTB("dhanashree");
					dr.hideKeyboard();
					cdm.setLastNameTB("hegde");
					dr.hideKeyboard();
					cdm.getRadioBtn().click();
					cdm.getRadioBtn().click();
					cdm.setDobTB("12/02/1993");
					dr.hideKeyboard();
					cdm.clickingselectGender();
					cdm.selectFema();
					cdm.setIdNumTB("452131595950");
					Thread.sleep(1000);

//					dr.hideKeyboard();
					cdm.setOTPTB("1234");
					dr.hideKeyboard();
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							cdm.getConfirmOTPBTN().click();
							try 
							{
								
								MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
								Reporter.log("Server response/Toast message is: "+toast.getText(),true);
		
								rm.takeScreenShot(dr);
							}
							catch (NoSuchElementException ex) 
							{
								Reporter.log("toast not displayed",true);
							}
							Thread.sleep(2000);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on confirm otp button",true);
					}
				}

				catch (Exception e) 
				{
					Reporter.log("Exception occured while entering customer detail manualy ",true);
				}
					//cdm.clickingVerifyBTN();
				tpft=new TripPassFromTo(dr);
					Thread.sleep(2000);
					try 
					{
						
						Reporter.log("Trip Pass page displayed",true);	
						try
						{
							if(tpft.getToAfterSelecting().isDisplayed())
									{
								    Reporter.log("Exception criteria "+rm.readExcel("AM-182",1,4 )+" Failed------->",true);
									}
							else
							{
								Reporter.log("Exception criteria "+rm.readExcel("AM-182",1,4 )+" Passed------->",true);
							}
						}
						catch (Exception e) {
							
						}
						String trip45=tpft.getMonthPAss45().getText();
						String trip60=tpft.getMonthPAss60().getText();
						String trip90=tpft.getMonthPAss90().getText();
						Reporter.log("Acceptance criteria "+rm.readExcel("AM-182",1,2 )+"Passed ------>",true);
						Reporter.log("Type of trip pass configured in system displayed as: "+trip45+"     "+trip60+"     "+trip90,true);	
						Thread.sleep(2000);
						tpft.getFirstFromStation().click();
						Reporter.log("Clicked on From station ",true);
						tpft.getSecondToStation().click();
						Reporter.log("Clicked on To station ",true);
						Reporter.log("Acceptance criteria "+rm.readExcel("AM-182",2,2 )+" Passed ------>",true);
						tpft.getMonthPAss60().click();
						Reporter.log("Clicked on Trip Pass Type ",true);
						Reporter.log("Acceptance criteria "+rm.readExcel("AM-182",3,2 )+" Passed ------>",true);
						Reporter.log("Pass amount displayed next to trip pass type selected as: "+tpft.getMonthPAss60amount().getText(),true);
						Reporter.log("Acceptance criteria "+rm.readExcel("AM-182",4,2 )+" Passed ------>",true);
						tpft.getNextBTN().click();
				   }
					catch(Exception e) 
					{
						Reporter.log("Exception in Trip Pass Page ",true);
					}
					pp=new PaymentPage(dr);
					try {
						if(pp.getConfirmBTN().isDisplayed())
						{
							Reporter.log("Payment Page displayed",true);
							
							pp.setCashReceived("1000");
							Reporter.log("change to be return "+ pp.getChangeField().getText(),true);
							pp.clickingConfirmBTN();
							Thread.sleep(1000);
							
							psp=new PaymentSuccessfulPage(dr);
							
							try
							{
								if(psp.getPaymentReceivedSuccessfullyMsg().isDisplayed()&&psp.getCardIssuedAndLinkedSuccessfullyMsg().isDisplayed()&&psp.getHostTopUpDoneSuccessfullyMsg().isDisplayed())
								{
									if(psp.getPaymentReceivedSuccessfullyMsg().getText().equals("Payment received successfully!")&&psp.getCardIssuedAndLinkedSuccessfullyMsg().getText().equals("Store Value Card issued and linked successfully!")&&psp.getHostTopUpDoneSuccessfullyMsg().getText().equals("Host top up done successfully!"))
									{
										Reporter.log("Message displayed as: "+psp.getPaymentReceivedSuccessfullyMsg().getText()+" "+psp.getCardIssuedAndLinkedSuccessfullyMsg().getText()+" "+psp.getHostTopUpDoneSuccessfullyMsg().getText(),true);
									try
									{
										Thread.sleep(3000);
										if(dr.findElement(By.xpath("//android.widget.TextView[@index='0']")).isDisplayed())
										{
											
											String cardlinkmsg=dr.findElement(By.xpath("//android.widget.TextView[@index='0']")).getText();
											
											if(cardlinkmsg.equalsIgnoreCase("Card linked successfully. Please wait while PIN is being set"))
											{
												
											Reporter.log("Message displayed as: "+cardlinkmsg,true);
											}
											else
											{
												
												Reporter.log("Card linked successfully. Please wait while PIN is being set Message NOT dispalyed",true);
											}
											Thread.sleep(50000);
											try
											{
												if(psp.getconfirmBTN().isDisplayed())
												{
													String passissuedmsg=tpft.getPinSetAndtripPassIssuedSuccessMsg().getText();
													
													if(passissuedmsg.equalsIgnoreCase("PIN set and Trip pass issued successfully"))
													{
													Reporter.log("Message displayed as: "+passissuedmsg,true);
													psp.getconfirmBTN().click();
													}
													else if(passissuedmsg.equalsIgnoreCase("Pass purchase failed"))
													{
													  Reporter.log("Message displayed as: "+passissuedmsg,true);
													  psp.getconfirmBTN().click();
													}
													else if(passissuedmsg.equalsIgnoreCase("PIN Set failed"))
													{
														Reporter.log("Message displayed as: "+passissuedmsg,true);
														psp.getconfirmBTN().click();
													}
												}
											}
											catch (NoSuchElementException e) {
												Reporter.log("PIN set and Trip pass not issued successfully ",true);
											}
										}
										
									}
									catch (NoSuchElementException e) 
									{
										Reporter.log("Acceptance Criteria "+rm.readExcel("AM-182",2,2 )+"Failed------>");
									}	
									
									
									
									
									}
									
									
									else
									{
										
										Reporter.log("Payment successful message not displayed",true);
										try 
										{
											pp.clickingConfirmBTN();
											MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
											String toastmsg=dr.findElementByXPath("//android.widget.Toast[1]").getText();
											if(toastmsg.equalsIgnoreCase("EDC not connected"))
											{
											Reporter.log("Server response/Toast message is: "+toastmsg,true);
											}
											else if((toastmsg.equalsIgnoreCase("Card linking failed")))
											{
												Reporter.log("Card linking failed",true);
												Reporter.log("Server response/Toast message is: "+toastmsg,true);
											}
											else
											{
												Reporter.log("Server response/Toast message is: "+toastmsg,true);
											}
											rm.takeScreenShot(dr);
										}
										catch (NoSuchElementException ex) 
										{
											Reporter.log("Payment successful message not displayed but tost message not displayed",true);
										}
										
									}
								}
								
							
							}
							catch (NoSuchElementException e)
							{
								Reporter.log("Payment successful message not displayed",true);
								try 
								{
									pp.clickingConfirmBTN();
									MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
									String toastmsg=dr.findElementByXPath("//android.widget.Toast[1]").getText();
									if(toastmsg.equalsIgnoreCase("EDC not connected"))
									{
										Reporter.log("Server response/Toast message is: "+toastmsg,true);
									}
									else if((toastmsg.equalsIgnoreCase("Card linking failed")))
									{
										Reporter.log("Card linking failed",true);
										Reporter.log("Server response/Toast message is: "+toastmsg,true);
									}
									else
									{
										Reporter.log("Server response/Toast message is: "+toastmsg,true);
									}
									rm.takeScreenShot(dr);
								}
								catch (NoSuchElementException ex) 
								{
									Reporter.log("edc toast msg not displayed",true);
								}

						   }
					}
					}
					catch (Exception e) {
						Reporter.log("Exception occured in payment page ",true);
					}
			}
		}
		catch (Exception e) {
			Reporter.log("Enter manually customer detail option not displayed",true);
		}
		
		

	}

}
