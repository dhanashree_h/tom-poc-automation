package scripts;

import org.testng.annotations.Test;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseLibrary.ReusableMethods;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pom.CustomerDetailsManually;
import pom.HomePage;
import pom.LoginPage;
import pom.PaymentPage;
import pom.PaymentSuccessfulPage;
import pom.StoreValuePage;

public class UserStoryAM115 {
	static AndroidDriver dr;
	HomePage hp;
	ReusableMethods rm;
	CustomerDetailsManually cdm;
	StoreValuePage stp;
	PaymentPage pp;
	PaymentSuccessfulPage psp;
	@Test
	public void call() throws MalformedURLException, InterruptedException 
	{
	DesiredCapabilities cap=new DesiredCapabilities();
	System.setProperty("webdriver.http.factory", "apache");
	cap=new DesiredCapabilities();
	cap.setCapability("platformName","Android");
	cap.setCapability("platformVersion","7");
	cap.setCapability("appPackage","com.paycraft.tom");
	cap.setCapability("appActivity","com.paycraft.tom.splash.SplashActivity");
	cap.setCapability("deviceName","T2mini");
	cap.setCapability("automationName","Appium");
	cap.setCapability("UDID","TN04197E40003");
	//cap.setCapability("autoGrantPermissions",true);
	dr= new AndroidDriver(new URL("http://localhost:4723/wd/hub"), cap);
	dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	LoginPage loginPage = new LoginPage(dr);
	rm=new ReusableMethods();
	loginPage.getMockoRadioBtn().click();
	loginPage.getProceedBtn().click();
	Thread.sleep(1000);
	hp=new HomePage(dr);
	rm=new ReusableMethods();
	Reporter.log("Login successful!",true);
	Reporter.log("--------User Story AM-115---------", true);
	try 
	{
		hp.getBluetoothIcon().click();
		hp.clickingEDC();
		try
		{
		hp.getCancelBTN().click();
		Reporter.log("Connected device name : "+hp.getBluetoothConnectedDeviceName().getText(), true);
		
		}
		catch (NoSuchElementException e) {
			Reporter.log("Exception occure while clicking on cancel button of bluetooth ",true);
		}

	}
	catch (NoAlertPresentException e) 
	{
		Reporter.log("Bluetooth connection alert not displayed",true);
	}
		try 
		{
			Thread.sleep(2000);
			hp.typeCRN(rm.getPropertyData("CRN"));
			dr.hideKeyboard();
			hp.typeMobileNumber(rm.getPropertyData("MobileNumberValid"));
			dr.hideKeyboard();
			try {
				if(hp.getSendOTPBTN().isDisplayed())
				{
					hp.clickingSendOTPBTN();
					Thread.sleep(2000);
				}
			} 
			catch (NoSuchElementException e) 
			{
				Reporter.log("send otp button not displayed",true);
			}
		}
		catch (Exception e) 
		{
			rm.takeScreenShot(dr);
			Reporter.log("Application is crashed",true);
		}
		

		try {
			rm=new ReusableMethods();
			if(hp.getManualEntryBTN().isDisplayed())
			{
				Reporter.log("Enter manually customer detail option displayed",true);
				hp.clickingManualEntryBTN();
				cdm=new CustomerDetailsManually(dr);
				try {

					Reporter.log("enter customer detail manualy page displayed",true);
					cdm.getDropDownArrow().click();
					cdm.getFirstNameDropDownOption().click();
					cdm.setFirstNameTB("dhanashree");
					dr.hideKeyboard();
					cdm.setLastNameTB("hegde");
					dr.hideKeyboard();
					cdm.getRadioBtn().click();
					cdm.getRadioBtn().click();
					cdm.setDobTB("12/02/1993");
					dr.hideKeyboard();
					cdm.clickingselectGender();
					cdm.selectFema();
					cdm.setIdNumTB("452131595950");
					try 
					{
						
						MobileElement toast=(MobileElement) dr.findElementByXPath("//android.widget.Toast[1]");
						Reporter.log("Server response/Toast message is: "+toast.getText(),true);

						rm.takeScreenShot(dr);
					}
					catch (NoSuchElementException ex) 
					{
						Reporter.log("",true);
					}
					dr.hideKeyboard();
					cdm.setOTPTB("1234");
					dr.hideKeyboard();
					try {
						if(cdm.getConfirmOTPBTN().isEnabled())
						{
							cdm.getConfirmOTPBTN().click();
							Thread.sleep(2000);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("not clicked on confirm otp button",true);
					}
				}

				catch (Exception e) 
				{
					Reporter.log("Exception occured while entering customer detail manualy ",true);
				}
					//cdm.clickingVerifyBTN();
					stp=new StoreValuePage(dr);
					Thread.sleep(2000);
					try 
					{
						
						Reporter.log("store value page displayed",true);
						Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",1 ,2 )+"Passed------>",true);
					Reporter.log("Preconfigured top up amounts displayed as "+stp.getBTN100().getText()+" "+stp.getBTN200().getText()+" "+stp.getBTN300().getText()+" "+stp.getBTN400().getText()+" "+stp.getBTN500().getText(),true);
					stp.clickingBTN100();
					Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",2 ,2 )+"Passed------>",true);
					Reporter.log("Amount set by selecting on Preconfigured topup amount is "+stp.getStoreValueField().getText(),true);
					stp.clickingClearBTN();
					stp.setStoreValueField("300");
					dr.hideKeyboard();
					Reporter.log("Amount set by entering topup amount is "+stp.getStoreValueField().getText(),true);
					
					String svbeforeplus=stp.getStoreValueField().getText();
					
					int s1=Integer.parseInt(svbeforeplus);
					System.out.println(s1);
					Reporter.log("Store value amount before clicking on '+' is: "+svbeforeplus,true);
					stp.getPlusBTN().click();
					String svafterplus=stp.getStoreValueField().getText();
					int s2=Integer.parseInt(svafterplus);
					Reporter.log("Store value amount after clicking on '+' is: "+svafterplus,true);
					if(s2-s1==100)
					{
						Reporter.log("Value is increased by 100");
						Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",3 ,2 )+"Passed------>",true);
					}
					else
					{
						Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",3 ,2 )+"Failed------>",true);
					}
					
					
					String svbeforeminus=stp.getStoreValueField().getText();
					int s3=Integer.parseInt(svbeforeminus);
					Reporter.log("Store value amount before clicking on '-' is: "+svbeforeminus,true);
					stp.getMinusBTN().click();
					String svafterminus=stp.getStoreValueField().getText();
					int s4=Integer.parseInt(svafterminus);
					Reporter.log("Store value amount after clicking on '-' is: "+svafterminus,true);
					if(s3-s4==100)
					{
						Reporter.log("Value is decreased by 100");
						Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",4 ,2 )+"Passed------>",true);
					}
					else
					{
						Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",3 ,2 )+"Failed------>",true);
					}
					stp.clickingClearBTN();
					stp.setStoreValueField("99");
					try 
					{
						if(stp.getPayBTN().isEnabled())
						{	
							Reporter.log("Pay button got enabled after entering amount less than 100 also",true);
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",5 ,2 )+"Failed------>",true);
						
						}
						else
						{
							Reporter.log("Pay button not got enabled after entering amount less than 100 ",true);
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",5 ,2 )+"Passed------>",true);
						}
					} 
					catch (Exception e) {
						
					}
					stp.clickingClearBTN();
//					String eligible=stp.getEligibleTopUpAmountSet().getText();
//					int eligibleamount=Integer.parseInt(eligible);
//					int maxeligibleamount=eligibleamount+1;
					//stp.setStoreValueField(Integer.toString(maxeligibleamount));
					stp.setStoreValueField("9999");
					try 
					{
						if(stp.getPayBTN().isEnabled())
						{	
							Reporter.log("Pay button got enabled after entering amount more than eligible top up amount also",true);
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",6 ,2 )+"Failed------>",true);
						
						}
						else {
							Reporter.log("Pay button not got enabled after entering amount more than eligible top up amount also ",true);
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",6 ,2 )+"Passed------>",true);
						}
					} 
					catch (Exception e) {
						
					}
					stp.clickingClearBTN();
					stp.setStoreValueField("");
					try 
					{
						if(stp.getPayBTN().isEnabled())
						{	
							Reporter.log("Pay button got enabled even though host amount is not entered",true);
							Reporter.log("Exception criteria----------"+rm.readExcel("AM-115",1 ,4 )+" Failed ------>",true);
						}
						else
						{
							Reporter.log("Pay button not got enabled if host amount is not entered",true);
							Reporter.log("Exception criteria----------"+rm.readExcel("AM-115",1 ,4 )+" Passed ------>",true);
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("set amount first",true);
					}
					stp.clickingClearBTN();
					stp.setStoreValueField("250");
					try 
					{
						if(stp.getPayBTN().isEnabled())
						{	
							Reporter.log("Pay button got enabled",true);
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",7 ,2 )+" Passed------>",true);
						stp.getPayBTN().click();
						}
					} 
					catch (Exception e) 
					{
						Reporter.log("set amount first",true);
						try 
						{
							stp.getPayBTN().click();
						}
						catch (Exception ex) 
						{
							Reporter.log("pay button not displayed",true);
						}
					}
					
					}
					catch(Exception e) 
					{
						Reporter.log("Exception in store value page ",true);
					}
					pp=new PaymentPage(dr);
					try {
						if(pp.getConfirmBTN().isDisplayed())
						{
							Reporter.log("Payment page displayed",true);
							Reporter.log("Acceptance Criteria---------- "+rm.readExcel("AM-115",8 ,2 )+" Passed------>",true);
							pp.clickingCashBTN();
							String totalamount=pp.getTotalAmountSet().getText();
							Reporter.log("Total amount displayed is "+totalamount);
							Thread.sleep(3000);
						}
					} 
					catch (Exception e) {
						Reporter.log("Exception occured in payment page ",true);
					}
			}
		}
		catch (Exception e) {
			Reporter.log("Enter manually customer detail option not displayed",true);
		}
	}

}
